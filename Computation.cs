﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using DwhProject.SikhDataSetTableAdapters;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;


namespace DwhProject
{
    class Computation
    {
        DBTalker db;
        string[] records;
        
        string file_name;
        char[] crit;//criteria for splitting
        string check;//for checking extension of file

        public string[] getRecords()
        {
            return this.records;
        }

        public DBTalker getdb()
        {
            return db;
        }
       
        public char[] getcrit()
        {
            return crit;
        }
        public Computation() 
        {
            db = new DBTalker();

        } 
        public void reader_excel(string fileName)
        {
            
            string[,] arr=new string[1,24];     //used for storing row of excel
            string[] temp;      //used for storing row of excel after converting into 1D from 2D
            int k=0, l=0;       //counters
            db = new DBTalker();//instance created of class that deals with database operations  CRUD
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@file_name);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            int rowCount = xlRange.Rows.Count;//rows in excel sheet
            int colCount = xlRange.Columns.Count;//coulumns in excel sheet 
            
            for (int i = 1; i < rowCount+1; i++)//looping through rows and coulumns of excel
            {
                
                for (int j = 1; j < colCount+1; j++)
                {
                        arr[k, l] =Convert.ToString(xlRange.Cells[i, j].value);
                   
                        l++;
                }
                l = 0;
               temp = convert_2d_1d(arr);//converting and storing in temp array
                db.inserter(temp);//inserting in database
                
            }
           

          

        }


        public bool checking_file_extension(string fileName)
        {
            check = Path.GetExtension(fileName);
            if(check==".txt")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        

       public void Retrieve(string file,char[] delimeter)
        {
            crit = delimeter;
            file_name = file;

            bool result = checking_file_extension(file_name);
           
           if (result==true)
            {
                ReadTextFile(file_name);//for reading txt file
            }
            else
            {
                reader_excel(file_name);//for reading excel file
               
            }
          
        }
       

        public void Method_Split(string line)
        {
            if (line != null)
            {
                string[] splitted_data = line.Split(crit);//splitting rows on basis of crit and storing in splitted_data
                db.inserter(splitted_data);//inserting splitted data in database
            }
           
        }


        public void ReadTextFile(String FileName)
        {
            int MAX = 2500000;
            String[] AllLines = new String[MAX];

            using (StreamReader SR = File.OpenText(FileName))
            {
                for (int i = 0; !SR.EndOfStream; i++)
                {
                    AllLines[i] = SR.ReadLine();
                }
            }
            for(int i=0;i<AllLines.Length;i++)
                {
                    if (AllLines[i] != null)
                    {
                        Method_Split(AllLines[i]);
                    }
                }
        }

        


        public void reader()
        {

            System.IO.StreamReader file = new System.IO.StreamReader(file_name);

            string line = null;

            while ((line = file.ReadLine()) != null)
            {
                Method_Split(line);

            }
        }

        public string[] convert_2d_1d(string[,] ar)
        {
            string[] temp=new string[24];
            int i = 0;
            for (int j = 0; j < 24;j++)
            {
                temp[j]=ar[i,j];
            }
            return temp;
        }


        public DWHAdapter show()//returns the adapter instance
        {
            
           return db.show_data();
        } 
    }
}
