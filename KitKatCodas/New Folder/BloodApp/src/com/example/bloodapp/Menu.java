package com.example.bloodapp;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Menu extends ListActivity{
	
	String classes[]={"my_profile","time_to_donate","diet_plan","share_points","invite_friends"};

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		
		super.onListItemClick(l, v, position, id);
		String cheese=classes[position];
		try{
			Class our_class=Class.forName("com.example.bloodapp."+cheese);
			Intent our_Intend=new Intent(Menu.this,our_class);
			startActivity(our_Intend);
			}catch(ClassNotFoundException e){
				e.printStackTrace();
			}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setListAdapter(new ArrayAdapter<String>(Menu.this, android.R.layout.simple_list_item_1, classes));
			}

}
