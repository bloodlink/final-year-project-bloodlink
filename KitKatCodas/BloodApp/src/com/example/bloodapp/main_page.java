package com.example.bloodapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class main_page extends Activity{

	public Button b1,b2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_page);
		b1=(Button)findViewById(R.id.mybutton2);
		b2=(Button)findViewById(R.id.Donor_Login_Button);
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent open_start_activity=new  Intent("com.example.bloodapp.MainActivity");
				startActivity(open_start_activity);
				
			}
		});
		b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent open_start_activity=new  Intent("com.example.bloodapp.Maps");
				startActivity(open_start_activity);
				
			}
		});
	}
	
	

}
