package com.example.bloodapp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	 int counter;
	 Button b1;
	 EditText input1,input2;
	 String check_input1,check_input2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		input1=(EditText)findViewById(R.id.rd_user_text);
		input2=(EditText)findViewById(R.id.rd_pass);
			b1=(Button) findViewById(R.id.rd_update_button);
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				check_input1=input1.getText().toString();
				check_input2=input1.getText().toString();
				if(check_input1.equals("D") || check_input2.equals("1"))
				{
					
					
					Thread timer=new Thread()
					{
						@Override
						public void run()
						{
								//Intent open_start_activity=new  Intent("com.example.bloodapp.Menu");
								Intent open_start_activity=new  Intent("com.example.bloodapp.Menu");
								startActivity(open_start_activity);
							
						}
						
					};
					timer.start();
					
				}
				
				else
				{
					//
				}
			}
		});
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
