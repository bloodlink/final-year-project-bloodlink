package organization.FAST.bigapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class CommunicatingService
{
	
public CommunicatingService() {
	// TODO Auto-generated constructor stub
}

public String get_xml_method() throws IllegalStateException, IOException{
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	StrictMode.setThreadPolicy(policy);
	BufferedReader in=null;
	String Data=null;
	StringBuffer sb=new StringBuffer("");
	HttpClient httpClient = new DefaultHttpClient();
	URI uri=null;
	try {
		uri = new URI("http://192.168.137.1/");
		
		
	} catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	HttpGet request = new HttpGet();
	request.setURI(uri);
	
	
	
	HttpResponse response = null;
	try {
		response = httpClient.execute(request);
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	in=new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	String l;
	String line_separator=System.getProperty("line.separator");
	while((l=in.readLine())!=null)
	{
		sb.append(l + line_separator);
	}
	in.close();
	Data=sb.toString();
	return Data;
}

}
