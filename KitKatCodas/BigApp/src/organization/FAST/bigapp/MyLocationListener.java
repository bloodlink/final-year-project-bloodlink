package organization.FAST.bigapp;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.Toast;
import android.app.Activity;
public class MyLocationListener extends Activity implements LocationListener {

	@Override
	public void onLocationChanged(Location loc) {
		// TODO Auto-generated method stub
		 loc.getLatitude();
	        loc.getLongitude();

	        String Text = "My current location is: " +
	        "Latitud = " + loc.getLatitude() +
	        "Longitud = " + loc.getLongitude();

	        Toast.makeText( getApplicationContext(), Text, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
	     
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
        Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}

}
