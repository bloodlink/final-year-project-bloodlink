This is a Final Year Project that I did for my Bachelor's Degree, the following is an "Executive Summary" for this project:


A patient is the priority of any hospital however due to the current blood replacement system operating in Pakistan’s hospitals and blood banks the patients tend to suffer the most. There is no regular donor base for the blood banks thus they are unable to meet the blood needs and are forced into using a demand-based model that requires patients to either arrange for blood on their own or give a bottle of blood in order to receive one in exchange. Even with this, there are many delays that have proven to be fatal.

For such a situation we have built BloodLink, a system that allows blood banks to have a strong donor base, to monitor their blood bank status, be timely informed of possible blood depletion, and have a network with blood donating organization and other blood banks. By offering such functionalities blood banks will be able to properly manage themselves and their blood donations which will ultimately aid the patients.

BloodLink is a web-based application using the ASP.Net framework. It will allow blood banks, regular donors and blood donating organizations to have their own profiles according to their required functionalities. Beside from common functionalities the blood banks will have an analysis module to Monitor the blood levels in blood banks, Blood depletion detection with alert generation for each blood bank and blood depletion prediction. The regular donors will be facilitated with a mobile application where their profile, notifications and reward points would be visible and shared.

This is a technical rich project since work was done on Android, Tableau and ASP.Net framework along with automated testing using Selenium framework; for that reason, the project uploaded is extremely messy, but changing the directories now would make a chaos for me so I'm sticking with it. :P 

A detailed report has also been attached with this project if the user wishes to gain further understanding of this project.