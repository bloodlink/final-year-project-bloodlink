﻿<%@ Page Title="Register Blood Donating Organization - Step 1" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="BDO.aspx.cs" Inherits="BloodLink.Account.BDO" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <div class="auto-style2">
        <h1><%: Title %>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h1></br>
        </div>
        <h3>Please create a profile before proceeding</h3>
    </hgroup>
    </br>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label1" runat="server" Text="Username: "></asp:Label>
        <asp:TextBox ID="txtname" runat="server"></asp:TextBox><asp:Label CssClass="field-validation-error" ID="ErrorName" runat="server" Text="This field can't be left empty" Visible="false"></asp:Label>
        </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
        <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox><asp:Label CssClass="field-validation-error" ID="Error2" runat="server" Text="This field can't be left empty" Visible="false"></asp:Label></br>
        <asp:Label ID="Label3" runat="server" Text="Confirm Password: "></asp:Label><asp:TextBox ID="txtconfirm" runat="server" TextMode="Password"></asp:TextBox><asp:Label CssClass="field-validation-error" ID="Error" runat="server" Text="The passwords do not match, please try again" Visible="false"></asp:Label>
        </br>

        </br>
        </br>
            <asp:Button ID="cmdproceed" OnClick="cmdproceed_Click" runat="server" Text="Proceed" />
    </p>

<%--    <asp:CreateUserWizard runat="server" ID="RegisterUser" ViewStateMode="Disabled" OnCreatedUser="RegisterUser_CreatedUser">
 </asp:CreateUserWizard>--%>
    </asp:Content>
