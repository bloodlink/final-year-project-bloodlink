﻿<%@ Page Title="Create a new event" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="BloodLink.Account.CreateEvent" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
            <h1> <%: Title %> </h1> <br />
            <h3>Create a new event</h3>
        </hgroup>
    <asp:Label ID="Label1" runat="server" Text="Name of the Event"></asp:Label>
    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Location of the Event"></asp:Label>
    <asp:TextBox ID="txtlocate" runat="server"></asp:TextBox>
    <br /> 
    <asp:Label ID="Label3" runat="server" Text="Time of the Event"></asp:Label>
    <asp:TextBox ID="txttime" runat="server"></asp:TextBox>
    <br /> 
    <asp:Label ID="Label4" runat="server" Text="Type of event"></asp:Label>
    <asp:TextBox ID="txttype" runat="server"></asp:TextBox>
    <br /> 
    <asp:Label ID="Label5" runat="server" Text="Approximate attendees"></asp:Label>
    <asp:TextBox ID="txtattend" runat="server"></asp:TextBox>
    <br /> <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Create" />
</asp:Content>
