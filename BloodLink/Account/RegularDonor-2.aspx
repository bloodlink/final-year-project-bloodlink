﻿<%@ Page  Title="Register Regular Donor - Step 2" MasterPageFile="~/Site.Master" Language="C#"  AutoEventWireup="true" CodeBehind="RegularDonor-2.aspx.cs" Inherits="BloodLink.Account.RegularDonor_2" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
<hgroup class="title">
        <div class="auto-style2">
        <h1><%: Title %>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h1></br>
        </div>
        <h3>Enter your details</h3>
    </hgroup>
     <p>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Label ID="Label1" runat="server" Text="First Name: "></asp:Label><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" runat="server" Text="Last Name: "></asp:Label><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp; <asp:Label ID="Label3" runat="server" Text="Father Name: "></asp:Label><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label4" runat="server" Text="Address: "></asp:Label><asp:TextBox ID="TextBox4" runat="server" OnTextChanged="TextBox4_TextChanged"></asp:TextBox>
         </br><asp:Label ID="Label5" runat="server" Text="Contact number: "></asp:Label><asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label6" runat="server" Text="Blood group: "></asp:Label><asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
         </br><asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click"/>
    </p>
</asp:Content>
