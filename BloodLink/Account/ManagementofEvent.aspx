﻿<%@ Page Title="Manage Event" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="ManagementofEvent.aspx.cs" Inherits="BloodLink.Account.ManagementofEvent" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section id="Manage">
   
       <aside>
           Need help for picking a place to camp?<br />
           Locate Donors on the basis of:<br />
           <asp:CheckBox ID="CheckBox1" runat="server" />
            <asp:Label ID="Label1" runat="server" Text="Blood Type"> </asp:Label><br />

           <asp:CheckBox ID="CheckBox2" runat="server" />
           <asp:Label ID="Label2" runat="server" Text="Area"></asp:Label><br />
           <asp:Button ID="Button1" runat="server" Text="Locate" OnClick="Button1_Click" />
        </aside>
        
         <hgroup class="title">
            <h1> <%: Title %> </h1> <br />
            <h3>Manage the events you've organized.</h3>
        </hgroup>
    <p>
    
        <ol>
           <li> <a href="/Account/CreateEvent.aspx">Create new event</a></li>
            <li><a href="/Account/PreviousEvent.aspx">View previous events</a></li>
            </ol>    
    </p>
    </section>
</asp:Content>