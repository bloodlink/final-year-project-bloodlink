﻿<%@ Page Title="Manage Event" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ManageEvent.aspx.cs" Inherits="FYPCode.Account.ManageEvent" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section id="Manage">
   
       <aside>
            Locate Donors on the basis of:<br />
            <asp:CheckBox ID="CheckBox1" Text="Blood Type" runat="server" />
            
            <asp:CheckBox ID="CheckBox2" Text="Area" runat="server" />
           <asp:Button ID="Button1" runat="server" Text="Locate" OnClick="Button1_Click" />
        </aside>
        
         <hgroup class="title">
            <h1> <%: Title %> </h1> <br />
            <h3>Manage the events you've organized.</h3>
        </hgroup>
    <p>
    
        <ol>
           <li> <a href="/Account/NewEvent">Create new event</a></li>
            <li><a href="/Account/Previous">View previous events</a></li>
            </ol>    
    </p>
    </section>
</asp:Content>