﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FYPCode;

namespace BloodLink.Account
{
    public partial class BloodBank : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cmdproceed_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtpassword.Text != "" && txtname.Text != "")
                {
                    if ((txtpassword.Text.Equals(txtconfirm.Text)) && (txtname.Text.Contains("@")))
                    {
                        DBTalker db_talker = new DBTalker();
                        string[] name = new string[] { txtname.Text, txtpassword.Text, "Regular Donor", null };
                        db_talker.insert("Profile", name);
                        String continueUrl = "/Account/BB-2.aspx";
                        Response.Redirect(continueUrl);
                    }
                    else
                    {
                        Error.Visible = true;
                    }
                }
                else if (txtpassword.Text != "")
                {
                    ErrorName.Visible = true;
                }
                else if (txtname.Text != "")
                {
                    Error2.Visible = true;
                }
                else
                {
                    ErrorName.Visible = true;
                    Error2.Visible = true;
                }
            }
            catch (Exception ex)
            { }
            
        }
    }
}