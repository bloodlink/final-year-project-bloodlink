﻿<%@ Page Title="Register Blood Bank - Step 2" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="BB-2.aspx.cs" Inherits="BloodLink.Account.BB_2" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <div class="auto-style2">
        <h1><%: Title %>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h1></br>
        </div>
        <h3>Enter all the details of your Blood Bank</h3>
    </hgroup>
    </br>
     <p>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:Label ID="Label1" runat="server" Text="Organization Name: "></asp:Label><asp:TextBox ID="txtname" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" runat="server" Text="Collaboration with (Hospital): "></asp:Label><asp:TextBox ID="txthospital" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label4" runat="server" Text="Address: "></asp:Label><asp:TextBox ID="txtaddress" runat="server"></asp:TextBox>
         </br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label5" runat="server" Text="Date of establishment: "></asp:Label><asp:TextBox ID="txtestablish" runat="server"></asp:TextBox>
         </br><asp:Label ID="Label6" runat="server" Text="Contact number (Tel & extension): "></asp:Label><asp:TextBox ID="txtphone" runat="server"></asp:TextBox>
         </br><asp:Label ID="Label7" runat="server" Text="How can you provide help with our projects vision: "></asp:Label><textarea id="TextArea1" cols="20" rows="6"></textarea>
         </br><asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click"/>
    </p>
    </asp:Content>