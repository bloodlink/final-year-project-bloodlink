﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FYPCode
{
    public class DBTalker
    {
        private Class1 obje;
        private SqlConnection conn;
        public DBTalker()
        {
            obje = new Class1();
            obje.connection();
            
           
        }


      public  void insert(String Table_Name,String[] parameters_for_coulumn_values)
        {
            try
            {
                conn = obje.get_connection();
                String query = obje.query_maker_insert(Table_Name, parameters_for_coulumn_values);
                SqlCommand quer = new SqlCommand(query, conn);
                quer.ExecuteReader();
                obje.close_connection();
            }
            catch (Exception e)
            { }
        }


      public void delete(String Table_Name, String Condition_to_delete)
        {
            conn = obje.get_connection();
            String query = obje.query_maker_delete(Table_Name,Condition_to_delete);
            SqlCommand quer = new SqlCommand(query, conn);
            quer.ExecuteReader();
            obje.close_connection();
        }

      public void update(String Table_Name, String[] parameters_for_coulumn_values,String condition_for_Update)
        {
            conn = obje.get_connection();
            String query = obje.query_maker_update(Table_Name,parameters_for_coulumn_values,condition_for_Update);
            SqlCommand quer = new SqlCommand(query, conn);
            quer.ExecuteReader();
            obje.close_connection();
        }

      public List<double[,]> return_values_of_table(String Table_Name)
      {
          conn = obje.get_connection();
          List<double[,]> mylist=new List<double[,]>();
          List<double[,]> lista = new List<double[,]>();
          double[,] Array;
          String[] Table_address_Location_string;
          String query = obje.query_maker_select(Table_Name);
            SqlCommand quer = new SqlCommand(query, conn);
          SqlDataReader reader = quer.ExecuteReader();
          if (Table_Name == "Pakistan")
          {
              while (reader.Read())
              {
                  Array = new double[1, 2];
                  Array[0, 0] = reader.GetFloat(1);
                  Array[0, 1] = reader.GetFloat(2);
                  lista.Add(Array);

              }
              reader.Close();
          }
          else if (Table_Name == "AAdress")
          {
              String Temp=null;
              lista.Clear();
              //Table_address_Location_string = new String();
              while (reader.Read())
              {

                //Temp = reader.GetString(2);
                 // lista.Add(Array);
                  Temp=reader["Location"]+" "+Temp;
              }
              reader.Close();
          }

          else
          {

          }
          //conn = obje.get_connection();
          return lista;
      }
    }
}