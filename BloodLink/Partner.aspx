﻿<%@ Page Title="Partners" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Partner.aspx.cs" Inherits="FYPCode.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1><br />
        
    </hgroup>
    <p><b>The following are the Blood Banks that have registered with our system:</b></p>
   <aside>
            <a href ="https://maps.google.com/maps?expflags=enable_star_based_justifications:true&ie=UTF8&cid=44232770213495903&q=Pims&iwloc=A&gl=PK&hl=en"><img alt="Map of the business location" src="Images/mappims.png" width="256" height="156" class="Nlc"></a>
        </aside>
    <section class="partner">
        <header>
            <h3>Pakistan Institute of Medical Sciences:</h3>
        </header>
        
            <h4>Adress:</h4>
        <p>
            Ibn-e-Sina Road, G-8/3, Islamabad.
        </p>
        <h4>Contact Number:</h4>
        <p>
            <span>+92 51 9261170</span>
        </p>
        <h4>Support:</h4>
        <p>
            <a href="mailto:info@pims.gov.pk">info@pims.gov.pk</a><br />
            <a href="http://www.pims.gov.pk/">www.pims.gov.pk</a>
        </p>
        
    </section>
    
</asp:Content>