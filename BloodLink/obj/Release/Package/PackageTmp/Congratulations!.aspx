﻿<%@ Page Title="Congratulations!" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Congratulations!.aspx.cs" Inherits="FYPCode.Congratulations_" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    
    
    <h2>Welcome to the BloodLink family</h2>
    <h3>
        Congratulations! You are now one of our registered users, to complete the process: kindly verify your provided email address by clicking on the link that has been sent to it.
    </h3>

    
</asp:Content>
