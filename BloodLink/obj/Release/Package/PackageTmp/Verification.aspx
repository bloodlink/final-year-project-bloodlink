﻿<%@ Page Title="Please wait." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Verification.aspx.cs" Inherits="FYPCode.Verification" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <hgroup class="title">
            <h1> <%: Title %> </h1> <br />

        <p>
            The event has been created: please wait untill the Blood Bank Administrator verifies it.

            <a runat="server" href="/Default">Return to main page.</a>
        </p>
        </hgroup>

</asp:Content>
