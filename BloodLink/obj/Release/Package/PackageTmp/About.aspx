﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="FYPCode.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1>Want to know more about us and the project?</h1><br />
    </hgroup>

    <article>
        <h2>Foresight</h2>
        <p>        
            Currently in Pakistan: a system even similar to ours has been deployed; and even internationally our system has much use for blood banks and organizations - such as American Red cross - that operates heavily in Pakistan. Pakistan’s need for a proper system for blood need is urgent and we recognized the need and decided to cater it through our skills.
        </p>

        <h2>Scope</h2>
        <p>  
            <ul>      
            <li>Blood Donation inventory</li>
            <li>Blood Bank depletion alert with specific report of requirement</li>
            <li>Blood donor Profile Management</li>
            <li>Blood Donation Emergency Management</li>
           <li> Blood Drive decision support system</li>
            <li>Localized/Specific blood donation collection </li>
           <li> Blood Bank depletion prediction</li>
        </ul></p>
        <h2>Features</h2>
        <p>        
            <ul>
            <li>Web based platform as main application and smartphone app for donors as supporting </li>
            <li>Online Donor cards with Blood type, screening test results and Medical history consolidated under one Unique Id</li>
            <li>Blood donation track - last time blood donated - will be achieved using clock timer on app </li>
            <li>Notifications to required donors for emergency blood donation (Location specified) and normal blood donation request (center, schedule and location specified through simple SMS)</li>
            <li>Notification to selected Blood donation centers to prepare for blood donation - blood group specified</li>
            <li>Evaluation and reports of donors on basis of area and organization</li>
            <li>Using evaluation reports to rank area and organization for required blood group</li>
            <li>Predictions for blood banks about the possible depletion of certain blood group</li>
            <li>Donor Score cards integrated with social media (Facebook and twitter) to encourage donors </li>
            <li>Learning algorithm gives accuracy of prediction for research purpose to help increase accuracy for depletion</li>
        </ul>
                </p>
    </article>

    <%--<aside>
        <h3>Aside Title</h3>
        <p>        
            Use this area to provide additional information.
        </p>
        <ul>
            <li><a runat="server" href="~/">Home</a></li>
            <li><a runat="server" href="~/About">About</a></li>
            <li><a runat="server" href="~/Contact">Contact</a></li>
        </ul>
    </aside>--%>
</asp:Content>