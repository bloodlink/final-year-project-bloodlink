﻿<%@ Page Title="Contact us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="FYPCode.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1><br />
        <h2>For any queries, you can contact us on the following:</h2>
    </hgroup>

    <section class="contact">
        <header>
            <h3>Phone:</h3>
        </header>
        <p>
            <span class="label">Naveed Iqbal - Supervisor:</span><br />
            0345-4057576
        </p>
        <p>
            <span class="label">Prof. Dr. Hasan Zaheer - Supervisor:</span><br />
            051-9261272
        </p>
        <p>
            <span class="label">Jam Jahanzeb - Team Lead:</span><br />
            0333-3417343
        </p>
        
    </section>

    <section class="contact">
        <header>
            <h3>Email:</h3>
        </header>
        <p>
            <span class="label">Support:</span>
            <span><a href="mailto:teenscheme@gmail.com">teenscheme@gmail.com</a></span>
        </p>
        <p>
            <span class="label">Marketing:</span>
            <span><a href="mailto:bilalcs201@gmail.com">bilalcs201@gmail.com</a></span>
        </p>
        <p>
            <span class="label">General:</span>
            <span><a href="mailto:jam.jahanzeb@gmail.com">jam.jahanzeb@gmail.com</a></span>
        </p>
    </section>

    <section class="contact">
        <header>
            <h3>Address:</h3>
        </header>
        <p>
            A.K. Brohi Road<br />
            H-11/4, Islamabad
        </p>
    </section>
</asp:Content>