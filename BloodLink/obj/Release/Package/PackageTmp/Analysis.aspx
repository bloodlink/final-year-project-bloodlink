﻿<%@ Page Title="Analysis" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Analysis.aspx.cs" Inherits="BloodLink.Analysis" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup>
        <h1><%: Title %></h1>
    </hgroup>
    <br /><br />

    <aside>
            Other options that you may use:
            <br />
            <br />
            To manage events, <a id="A1" runat="server" href="~/Account/ManagementofEvent.aspx"> click here.<br /></a>
            <br />
            To break into zones, <a id="A2" runat="server" href="~/MapCountry.aspx"> click here.<br /></a><br />
            <br />
            Get back to managing you account, <a id="A3" runat="server" href="~/Account/Management.aspx"> click here.<br /></a><br />

        </aside>

    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Data refinement" Width="212px" />  Perform ETL process on the Data which is present in your system
    <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Analysis" Width="211px" />  Perform analysis to get insight for Camps
    <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Notifications" Width="213px" />  Send notifications to the Regular Donors
    <br />
    </asp:Content>
