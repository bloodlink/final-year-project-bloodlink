﻿<%@ Page Title="Project Plan" Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="ProjectPlan.aspx.cs" Inherits="BloodLink.ProjectPlan" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1>Project Plan</h1> <br />
        <h2>The overview of our project.</h2>
    </hgroup>

    <section class="projectplan">
        <header>
            <h3>Vision:</h3>
        </header>
        <p>
            To eliminate the traditional Demand-based model - which is currently being used - with the Supply-based model
        </p>
    </section>

    <section class="projectplan">
        <header>
            <h3>Mission:</h3>
        </header>
        <p>
            To integrate all the blood banks of Pakistan and make a bridge between the Blood Donating Organizations and<br /> Blood banks 
                to ensure safe donations from the donors who are registered with our system
        </p>
    </section>

    <section class="projectplan">
        <header>
            <h3>Plan of execution:</h3>
             <img src="Images/systemflow-balck.PNG" style="height: 445px; width: 768px">&nbsp;
        </header>
        
        <ol class="round">
        <li class="one">
            <h5>The donor will register with our system</h5> - <a id="A2" runat="server" href="~/Account/Register.aspx">register now</a>
        </li>
        <li class="two">
            <h5>The donor will have to get his/her blood screened from one of our registered Blood banks</h5>- <a id="A1" runat="server" href="~/Partner.aspx">see registered Blood banks</a>
        </li>
        <li class="three">
            <h5>Now the user is one of our registered donor</h5>- already a member? <a id="A3" runat="server" href="~/Account/Login.aspx">Login</a>
        </li>
        <li class="four">
           <h5> Donor will be alerted:</h5>- in case of blood falls below a particular level<br />- when a blood camp is put up near their location<br />- when a particular season of deficiency is predicted (through analytical processing)
        </li>
        <li class="five">
           <h5> Analysis will be done on the donation information:</h5>- to predict seasonal deficiecy<br />- to study the patterns of areas with most donations<br />- to group the regions of Pakistan according to majority of the blood type donated from
        </li>
    </ol>

        
    </section>
</asp:Content>