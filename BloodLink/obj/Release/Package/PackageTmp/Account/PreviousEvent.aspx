﻿<%@ Page Title="Details of Previous events" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="PreviousEvent.aspx.cs" Inherits="BloodLink.Account.PreviousEvent" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
            <h1> <%: Title %> </h1> <br />
            <h3>Details of the events you've organized.</h3>
        </hgroup>
</asp:Content>