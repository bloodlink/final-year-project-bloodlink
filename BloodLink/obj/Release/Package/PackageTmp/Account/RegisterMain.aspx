﻿<%@ Page Title="Registeration" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="RegisterMain.aspx.cs" Inherits="BloodLink.Account.RegisterMain" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h1><%: Title %></h1>
    <h3>Which of the following do you wish to register in:</h3>
    <ol>
        <li>
            <a href="/Account/RegularDonor.aspx">Regular Donor</a>
        </li>
        <li>
            <a href="/Account/BloodBank.aspx">Blood Bank</a>
        </li>
        <li>
            <a href="/Account/BDO.aspx">Blood Donating Organization</a>
        </li>
    </ol>
</asp:Content>