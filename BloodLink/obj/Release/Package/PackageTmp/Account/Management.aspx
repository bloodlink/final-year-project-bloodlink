﻿<%@ Page Title="Management" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Management.aspx.cs" Inherits="BloodLink.Account.Manage" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section id="Manage">
   
        <aside>
            Other options that you may use:
            <br />
            <br />
            To manage events, <a runat="server" href="~/Account/ManagementofEvent.aspx"> click here.<br /></a>
            <br />
            To break into zones, <a runat="server" href="~/MapCountry.aspx"> click here.<br /></a><br />
            Click the button below to do analysis:<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Do Analysis" />

        </aside>
        
         <hgroup class="title">
            <h1> <%: Title %> </h1> <br />
            <h3>Manage you account.</h3>
        </hgroup>
        <p>
        Update personal information:<br />
        <a id="A1" runat="server" href="~/Account/ChangePassword"><img src="Images/change_password.png" style="height: 100px; width: 100px"></a><a id="A2" runat="server" href="~/Account/ManageDetailsofRD.aspx"><img src="Images/updateinfo.png" style="height: 100px; width: 100px"></a><br />
        <br />Download Smartphone Application<br /><a href="https://www.dropbox.com/s/2tv947c6fe1k25r/BloodApp.rar"><img src="Images/Android.png" style="height: 100px; width: 100px"></a><a href="~/"><img src="Images/windows-phone-8-logo.png" style="height: 100px; width: 100px"></a><br />
    </p>
    </section>
</asp:Content>