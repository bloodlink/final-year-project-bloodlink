﻿<%@ Page Title="Home" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Default.aspx.cs" Inherits="BloodLink.Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>BloodLink</h1><br /><br />
                <h2>Bridging the gap between Blood donors and Blood banks.</h2>
            </hgroup>
            <p>
                In Pakistan health care is a sector that is in dire need of attention. Currently the procedure used in blood banks is a Demand based model which bottle vs. bottle. This model causes further duress to the patients and a delay in blood could prove to be extremely harmful even fatal.
                <br /> As many developed countries have already replaced Demand based model with supply based model it is our aim as well, to change the current norm for blood donations in blood banks and hospitals of Pakistan.</p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>How it works:</h3>
    <ol class="round">
        <li class="one">
            <h5>Getting Started</h5>
            Register yourself with our website
        </li>
        <li class="two">
            <h5>Screening</h5>
            Get your blood sample screened and verified for one of our registered blood banks
        </li>
        <li class="three">
            <h5>Donate</h5>
            Now you are one of our prestigious donor and can avail the many opportunities that come from it.
        </li>
    </ol>
    <p>
        <a id="A1" runat="server" href="~/ProjectPlan.aspx">Click here</a> to get the complete project plan.
    </p>
</asp:Content>