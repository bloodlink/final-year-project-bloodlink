﻿<%@ Page  Title="Show map"  Language="C#" AutoEventWireup="true" CodeBehind="MapCountry.aspx.cs" Inherits="BloodLink.MapCountry" %>

<!DOCTYPE HTML>
<html >
    <head >
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <meta charset="utf-8">

    <style type="text/css" runat="server">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 100% }
    </style>
    <script  type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
    </script>
    <script type="text/javascript" >
        var map;
        var geocoder;

        var array_for_map_boundary = new Array();
        var x = new google.maps.LatLng(52.395715, 4.888916);
        var stavanger = new google.maps.LatLng(33.702313, 73.053949);
        var amsterdam = new google.maps.LatLng( 23.70514,68.07542);
        var london = new google.maps.LatLng(23.70514, 68.07514);
        function initialize() {
            geocoder = new google.maps.Geocoder();
            var myLatLng = new google.maps.LatLng(33.702313, 73.053949);
            var mapOptions = {
                center: myLatLng,

                zoom: 8
            };
            map = new google.maps.Map(document.getElementById("map-canvas"),
                mapOptions);

            var marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: "pims hospital islamabad"
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<img src="PimsImage.jpg" ><br> Pims Hospital, G-8/4 ,Islamabad</br>',
                position: myLatLng,
                
            });

            google.maps.event.addListener(marker, 'click', function () {
                map.setZoom(8);
                map.setCenter(marker.getPosition());
                infowindow.open(map);
            });
            alert("MapCircle");

            google.maps.event.addListener(map, 'click', function (event) {
                var citymap = {};
                citymap['chicago'] = {
                    center: new google.maps.LatLng(31, 72),
                    population: 2842518
                };
                citymap['newyork'] = {
                    center: new google.maps.LatLng(30.12, 67.01),
                    population: 2842518
                };
                citymap['losangeles'] = {
                    center: new google.maps.LatLng(26.1, 68.5),
                    population: 2842518
                };
                citymap['khyber'] = {
                    center: new google.maps.LatLng(34.00, 71.32),
                    population: 2842518
                };
                var cityCircle;
                for (var city in citymap) {
                    var populationOptions = {
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center: citymap[city].center,
                        radius: citymap[city].population / 15
                    };
                    // Add the circle for this city to the map.
                    cityCircle = new google.maps.Circle(populationOptions);
                    alert("MapCircleEnded");
                }


            });
            

            google.maps.event.addListener(map, "click", function (e) {
                alert(e.latLng.lat() + ' ' + e.latLng.lng());
                //optionally mark the location
                markLocation(e.latLng);
            });



        }

        function markLocation(userLocation) {

            if (!marker) {

                marker = new google.maps.Marker({
                    position: userLocation,
                    map: map,
                    title: "Chosen location"
                });

            } else {

                //update the marker position if it's already been set
                marker.setPosition(userLocation);

            }

        }

        function setmarker(params) {
            alert("Set marker started"+params);
            var xx = new Array(2);
            for (var i = 0; i < params.length; i++) {
                xx = params[i].split(",");
                //var y = new google.maps.LatLng(params[i, 0], [i, 1]);
                var y = new google.maps.LatLng(xx[1], xx[0]);
                
                //array_for_map_boundary[i]=y;
                //array_for_map_boundary.push(new google.maps.LatLng(xx[1], xx[0]));
                 array_for_map_boundary[i]=y;
                
                
                if (i == 0) {
                    alert(y);
                    alert(array_for_map_boundary[0]);
                }

                

                
            }
            alert("Loop Ended " );
            //alert("Hello Boss" + params);
            //var myTrip = [stavanger, amsterdam, london];
            var myTrip = [new google.maps.LatLng(33.702313, 73.053949), array_for_map_boundary[9999]];
            //alert(myTrip);
            var flightPath = new google.maps.Polygon({
                path: array_for_map_boundary,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2
            });

            flightPath.setMap(map);

        }
        
        function Alpha(x) {
         
            alert("Alpha Called");
            var someProp = "<% =my_temp %>";
            alert(someProp);
            var txt = new Array();
            txt = someProp.split(" ");
           
            initialize();
            setmarker(txt);
            //array_for_map_boundary.push(new google.maps.LatLng(x,y));
            //alert(new google.maps.LatLng(x, y));
            //initialize();
            //setmarker(params);
        }

        function Locate_Donors()
        {
            var address = document.getElementById("address").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });

            
        }
        //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
        
        
  </head>
  <body >
      
      <form id="form1" runat="server" style="width:505px; height:383px;">
      
    <div id="map-canvas" style="width:500px; height:380px;">
        </div>
          <div id="pol">
              <input id="address" type="textbox" value="Sydney, NSW">
      <input type="button" value="Geocode" onclick="Locate_Donors()">

          </div>
          </form>
  </body>

</html>
