﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Class1
/// </summary>
public class Class1
{
    private string colNames;
    List<String[]> mylist;
    string Connection ="Data Source=(LocalDB)\\v11.0;AttachDbFilename=F:\\Semester7\\FYP-1\\CODE\\FYPCode\\FYPCode\\aspnet-FYPCode-20131006162703c.mdf;Integrated Security=True;Connect Timeout=30";
    SqlConnection con;
	public Class1()
	{
        mylist = new List<string[]>();
        this.Adding_in_lists();
	}

    //these are the tables that are being used in the database
    String[] Profile = {"Profile_ID", "UserName","UserPassword",  "Type",  	"Status"};
    String[] AAdress={ "ProfileID",	"CityID" ,	"Location"};
    String[] City = {"ID","Name"};
    String[] RD = { "RD_ID", "Blood_ID", "Profile_ID", "Name", "Age","PhoneNumber" };
    String[] Screening = { "ScreeningID", "BloodBank_ID", "RD_ID", "Date" };
    String[] BloodDonatingOrganizations={"BDO_ID" ,	"Profile_ID"  ,	"Name"  ,"PhoneNumber" ,	"EstablishmentDate"  ,"NumberOfDonors" };
    String[] BloodBank={"BB_ID", "Profile_ID","Name","PhoneNumber" ,"EstablishmentDate" ,"Collaborating" ,"Hospital" };
    String[] BloodLevels={"BB_ID" ,"BloodType_ID" ,"level"};
    String[] Events={"Event_ID","Creator_ID","Invite_ID"};
    String[] Invites={"Invite_ID","Profile_ID" ,"Event_ID" ,"Status"};
    String[] EventDetails={"Event_ID","Location","Time","Name","Type","Number_OF_Attendees"};
    String[] FeedBack={ "F_ID", "Event_ID","Profile_ID","Comments","Rating"};
    String[] BloodType={"ID" ,"Name"};
    String[] Pakistan = { "ID", "POINT_X","POINT_Y","geom"};
    String[] TableNames = { "Screening", "RD", "City", "AAdress", "BloodDonatingOrganizations", "Profile", "BloodBank", "BloodLevels", "Events", "BloodType", "FeedBack", "EventDetails", "Invites", "Pakistan"};

    public string connection() 
    {
        string abc=null;
        con=new SqlConnection(Connection);
        return abc;
}

    public void Adding_in_lists()
    {
        mylist.Add(Screening);
        mylist.Add(RD);
        mylist.Add(City);
        mylist.Add(AAdress);
        mylist.Add(BloodDonatingOrganizations);
        mylist.Add(Profile);
        mylist.Add(BloodBank);
        mylist.Add(BloodLevels);
        mylist.Add(Events);
        mylist.Add(BloodType);
        mylist.Add(FeedBack);
        mylist.Add(EventDetails);
        mylist.Add(Invites);
        mylist.Add(Pakistan);
        
    }

    string attach(string temp,ArrayList obj)
    {
        for (int i = obj.Count-1; i >=0; i--)
        {
            temp = obj[i].ToString() + temp;
        }
        return temp;
    }

    string Append_Columns(string []Array_Coulums)
    {
        string temp = null;
        ArrayList array = new ArrayList();
        
        for (int i = 0; i < Array_Coulums.Length;i++)
        {
            array.Add(Array_Coulums[i]);
            array.Add(",");
        }
        array.RemoveAt(array.Count-1);

        return temp = attach(temp, array);
    }

    string Append_Columns(string[] Array_Coulums,int a)
    {
        string temp = null;
        ArrayList array = new ArrayList();

        for (int i = 0; i < Array_Coulums.Length; i++)
        {
            if (i== 0 && a==1)
            {
                
            }else if(a==2)
            {
                array.Add("'"+Array_Coulums[i]+"'");
                array.Add(",");
            }else {
                array.Add(Array_Coulums[i]);
                array.Add(",");
            }
        }
        array.RemoveAt(array.Count-1);

        return temp = attach(temp, array);
    }

    string Append_Columns(string[] Array_Coulums, string[] values_of_coulum)
    {
        string temp = null;
        ArrayList array = new ArrayList();

        int j = 0;

        for (int i = 0; i < Array_Coulums.Length; i++)
        {
            j = i - 1;
            if (j >= 0)
            {
                array.Add("[" + Array_Coulums[i]+"]" + "=" +"'"+ values_of_coulum[j] + "'");
                array.Add(",");
            }
        }
        array.RemoveAt(array.Count - 1);
        return temp = attach(temp, array);
    }
    
    public SqlConnection get_connection()
    {


        if (con.State == ConnectionState.Open)
        {
            
        }
        else
        {
            con.Open();

        }
        return con;
    }

    public void close_connection()
    {
        con.Close();
    }

    String check(string name)
    {
        int index = Array.IndexOf(TableNames,name);
        string query_data=null;
        return query_data = Append_Columns(mylist[index]);
        
    }

    String check(string name,int a)
    {

        int index = Array.IndexOf(TableNames, name);
        string query_data = null;
            return query_data = Append_Columns(mylist[index], a);
    }

    String check(string name, string[] array_colm_values)
    {
        int index = Array.IndexOf(TableNames, name);
        string query_data = null;
        return query_data = Append_Columns(mylist[index], array_colm_values);
        
    }

    public String get_coulumns()
    {
        return colNames;
    }

    public string query_maker_select(string TableName)
    {
        
         colNames=check(TableName);
        string new_query="select "+colNames+" from "+TableName;
        return new_query;
    }

    public string query_maker_insert(string TableName,string[] values)
    {
        
         colNames = check(TableName,1);
        string new_query = "INSERT INTO  " + TableName+" (" + colNames+") "+" VALUES( "+Append_Columns(values,2)+" )";
           
        
        return new_query;
    }

    public string query_maker_update(string TableName, string[] values,string condition) 
    {
        string colNames = check(TableName, values);
        string new_query = "UPDATE  " + TableName + " SET " + colNames  + " WHERE " + condition;
        return new_query;      
    }

    public string query_maker_delete(string TableName,string condition)
    {
        string query= "DELETE FROM "+ TableName +" WHERE "+ condition;
        return query;
    }
}