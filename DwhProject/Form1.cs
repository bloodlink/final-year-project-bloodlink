﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using chart = System.Windows.Forms.DataVisualization.Charting.SeriesChartType;
using DwhProject;
using FYPCode;

namespace DwhProject
{
    public partial class Form1 : Form
    {

        List<double> yValues = new List<double>();
        List<string> xNames=new List<string>();

        public Form1()
        {
            InitializeComponent();
            Add();
        }

        void Add()
        {
            comboBox1.Items.Add(chart.Area);
            comboBox1.Items.Add(chart.Bar);
            comboBox1.Items.Add(chart.BoxPlot);
            comboBox1.Items.Add(chart.Bubble);
            comboBox1.Items.Add(chart.Candlestick);
            comboBox1.Items.Add(chart.Column);
            comboBox1.Items.Add(chart.Doughnut);
            comboBox1.Items.Add(chart.ErrorBar);
            comboBox1.Items.Add(chart.FastLine);
            comboBox1.Items.Add(chart.FastPoint);
            comboBox1.Items.Add(chart.Funnel);
            comboBox1.Items.Add(chart.Kagi);
            comboBox1.Items.Add(chart.Line);
            comboBox1.Items.Add(chart.Pie);
            comboBox1.Items.Add(chart.Point);
            comboBox1.Items.Add(chart.PointAndFigure);
            comboBox1.Items.Add(chart.Polar);
            comboBox1.Items.Add(chart.Pyramid);
            comboBox1.Items.Add(chart.Radar);
            comboBox1.Items.Add(chart.Range);
            comboBox1.Items.Add(chart.RangeBar);
            comboBox1.Items.Add(chart.RangeColumn);
            comboBox1.Items.Add(chart.Renko);
            comboBox1.Items.Add(chart.Spline);
            comboBox1.Items.Add(chart.SplineArea);
            comboBox1.Items.Add(chart.SplineRange);
            comboBox1.Items.Add(chart.StackedArea);
            comboBox1.Items.Add(chart.StackedArea100);
            comboBox1.Items.Add(chart.StackedBar);
            comboBox1.Items.Add(chart.StackedBar100);
            comboBox1.Items.Add(chart.StackedColumn);
            comboBox1.Items.Add(chart.StackedColumn100);
            comboBox1.Items.Add(chart.StepLine);
            comboBox1.Items.Add(chart.Stock);
            comboBox1.Items.Add(chart.ThreeLineBreak);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  xNames.Add(checkedListBox1.CheckedItems.Count.ToString());
           // yValues.Add(12);
            //chart1.Series[0].ChartType=System.Windows.Forms.DataVisualization.Charting.SeriesChartType;
            if (comboBox1.SelectedItem == null)
            {
                chart1.Series[0].ChartType = chart.Pie;
            }
            else
            {
                chart1.Series[0].ChartType = (chart)comboBox1.SelectedItem;
            }
            chart1.Series[0].Points.DataBindXY(xNames, yValues);
        }

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        void check_the_box(CheckBox cb,int value)//for checking checkbox clicked and not clicked
        {
            if(cb.Checked)
            {
                xNames.Add(cb.Text);//adding in the names category list 
                yValues.Add(value);////adding in the values category list
                chart1.Series[0].Points.DataBindXY(xNames, yValues);//initializing chart with x,y values
            }

            else
            {
                xNames.Remove(cb.Text);
                yValues.Remove(10);
                chart1.Series[0].Points.DataBindXY(xNames, yValues);
 
            }
        }

        string local_query_maker(CheckBox cb)
        {
            return "Select count(*) from BloodType where Name = '"+cb.Text+"' ";
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox1);
            int count=obj.query(s);
            check_the_box(checkBox1,count);
 
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox2);
            int count = obj.query(s);
            check_the_box(checkBox2,1);
 
        }

        

        

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox3);
            int count = obj.query(s);
            check_the_box(checkBox3,1);
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox4);
            int count = obj.query(s);
            check_the_box(checkBox4,1);
        }

        
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox7);
            int count = obj.query(s);
            check_the_box(checkBox7,1);
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            DataBaseTalker obj = new DataBaseTalker();
            String s = local_query_maker(checkBox8);
            int count = obj.query(s);
            check_the_box(checkBox8,1);
        }

    }
}