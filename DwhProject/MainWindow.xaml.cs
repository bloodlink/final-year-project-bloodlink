﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using DwhProject.SikhDataSetTableAdapters;
using System.Data.SqlClient;
using System.Data;

namespace DwhProject
{
    
    public partial class MainWindow : Window
    {

        TransformationController TController = new TransformationController();

        StagObject MyStag = new StagObject();

        Computation obj;
        string FileName;
        char [] Delim;

        /*void setColmName()
        {
            MyStag.TableAllColmNames = new String[grig.Columns.Count];

            for (int i = 0; i < grig.Columns.Count; i++)
            {
                this.MyStag.TableAllColmNames[i] = grig.Columns[i].Header.ToString();
            }
        }
         * */


        String[] Datecolms = new String[] { "SowingDate", "VisitDate", "PesticideSpraydate" };

        

        int[] Array;

        List<int> myDistinctList;
        List<int> myNotNullList;
        List<int> myNullList;
        List<int> myInvalidList;
        List<double> myMinList;
        List<double> myMaxList;
        List<double> myAvgList;
        List<DateTime> myMinDatelist;
        List<DateTime> myMaxDatelist;



        public MainWindow()
        {
            InitializeComponent();
            obj = new Computation();

            ComboBox_ProfilingTable.Items.Add("STAG");
            ComboBox_ProfilingTable.Items.Add("Table");


        }

        private void adding_in_list(int[] arr)
        {
            listview.Items.Clear();

            for (int i = arr.Length - 1; i >= 0; i--)
            {

                listview.Items.Add(MyStag.TableAllColmNames[i] + "\t" + " " + arr[i]);
            }

        }


        private void adding_in_list(double[] arr, int u)
        {
            listview.Items.Clear();

            for (int i = arr.Length - 1; i >= 0; i--)
            {
                listview.Items.Add(MyStag.TableAllColmNames[i] + "\t" + " " + arr[i]);
            }

        }

        private void LoadingInLists(String TableName)
        {
            
            myDistinctList = obj.getdb().GetDistinctArray(MyStag.TableAllColmNames, TableName);
            myNotNullList = obj.getdb().GetNotNULLArray(MyStag.TableAllColmNames, TableName);
            myNullList = obj.getdb().GetNULLArray(MyStag.TableAllColmNames, TableName);
            myInvalidList = obj.getdb().GetInvalidArray(MyStag.TableAllColmNames, TableName);
            myMinList = obj.getdb().GetMinArray(MyStag.TableAllColmNames, TableName);
            myMaxList = obj.getdb().GetMaxArray(MyStag.TableAllColmNames, TableName);
            myAvgList = obj.getdb().GetAvgArray(MyStag.TableAllColmNames, TableName);
            myMinDatelist = obj.getdb().GetMinDateArray(Datecolms, TableName);
            myMaxDatelist = obj.getdb().GetMaxDateArray(Datecolms, TableName);

            LoadProfileListInProfileTable();
        }


        private void LoadProfileListInProfileTable()
        {
            TController.LoadProfileListInProfileTable(myDistinctList,myNotNullList, myNullList, myInvalidList,myMinList,myMaxList,myAvgList, myMinDatelist,myMaxDatelist);
        }

        private void LoadBarChartLists(List<int> CurrentList)
        {

             /*myDistinctList = obj.getdb().GetDistinctArray(MyStag.TableAllColmNames);
             myNotNullList = obj.getdb().GetNotNULLArray(MyStag.TableAllColmNames);
             myNullList = obj.getdb().GetNULLArray(MyStag.TableAllColmNames);
             myInvalidList = obj.getdb().GetInvalidArray(MyStag.TableAllColmNames);
             
            Array = myDistinctList.ToArray();*/
            Array = CurrentList.ToArray();
            adding_in_list(Array);

            List<KeyValuePair<string, int>> MyPairs = new List<KeyValuePair<string,int>>();

                for(int i=0 ; i<MyStag.TableAllColmNames.Length; i++)
                {
                    MyPairs.Add(new KeyValuePair<string,int>(MyStag.TableAllColmNames[i], Array[i]));
                }

                ((BarSeries)mcChart.Series[0]).ItemsSource= MyPairs;
        }



        private void LoadBarChartLists(List<double> CurrentList,int u)
        {
            double[] double_Array;
            /*myDistinctList = obj.getdb().GetDistinctArray(MyStag.TableAllColmNames);
            myNotNullList = obj.getdb().GetNotNULLArray(MyStag.TableAllColmNames);
            myNullList = obj.getdb().GetNULLArray(MyStag.TableAllColmNames);
            myInvalidList = obj.getdb().GetInvalidArray(MyStag.TableAllColmNames);
             
           Array = myDistinctList.ToArray();*/
            double_Array = CurrentList.ToArray();
            adding_in_list(double_Array,u);

            List<KeyValuePair<string, double>> MyPairs = new List<KeyValuePair<string, double>>();

            for (int i = 0; i < MyStag.TableAllColmNames.Length; i++)
            {
                MyPairs.Add(new KeyValuePair<string, double>(MyStag.TableAllColmNames[i], double_Array[i]));
            }

            ((BarSeries)mcChart.Series[0]).ItemsSource = MyPairs;
        }


         
        private void Enable_Radio_Button()
        {
            NullRadioButton.IsEnabled =true;
            NotNullRadioButton.IsEnabled = true;
            MaxRadiobutton.IsEnabled = true;
            MinRadioButton.IsEnabled = true;
            AverageRadioButton.IsEnabled = true;
            DistinctRadioButton.IsEnabled = true;
            InvalidRadioButton.IsEnabled=true;
            
        }

        Microsoft.Win32.OpenFileDialog dlg;
        private void b1_Click(object sender, RoutedEventArgs e)
        {
        
            
            List<String> my = new List<string>();

            dlg = new Microsoft.Win32.OpenFileDialog();//dialog box for selecting file

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt|(.xls)|*.xls";//filters for file types
            dlg.Multiselect = true;
            Nullable<bool> result = dlg.ShowDialog();
            
        }

        private void DistinctRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myDistinctList);

        }

        private void NotNullRadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myNotNullList);
        }

        private void NullRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myNullList);
        }

        private void MinRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myMinList,1);
        }

        private void MaxRadiobutton_Checked(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myMaxList,1);
        }

        private void AverageRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myAvgList,1);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            new Loading().Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //obj.getdb().BulkInsert();
            obj.getdb().PROC();
        }

        private void Button_Multiple_Click(object sender, RoutedEventArgs e)
        {
            new Transformation(MyDataTable).Show();
            this.Close();
        }

        private void Button_Show_Click(object sender, RoutedEventArgs e)
        {
            LoadingInLists(ComboBox_ProfilingTable.Text);
            Enable_Radio_Button();
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            LoadBarChartLists(myInvalidList);
        }


//        DataTable dt = new DataTable("STAG TABLE");


        public void ReadnTransfomFile(char[] Delimeter, String [] FileNames)
        {
            String[] Delim = new String[] { "S/O", "s/o", "S/o", "s/O" };
            

            for(int i=0; i<FileNames.Length;i++)
            {
                using (FileStream fs = File.Open(FileNames[i],FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (StreamReader sr = new StreamReader(bs))
                {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            String [] data_objects = s.Split(Delimeter);

                            MyStag.DistrictName = data_objects[0].Trim().ToUpper();
                            MyStag.TownName = data_objects[1].Trim().ToUpper();
                            String[] FarmerName = data_objects[2].Trim().Split(Delim, StringSplitOptions.None);

                            MyStag.FarmerFirstName = FarmerName[0].Trim().Split(' ')[0].Trim().ToUpper();
                            
                            MyStag.FarmerLastName = FarmerName[0].Trim().Substring(FarmerName[0].IndexOf(' ')).Trim().ToUpper();

                            string [] father = FarmerName[1].Split(' ');



                            MyStag.FatherFirstName = FarmerName[1].Trim().Split(' ')[0].Trim().ToUpper();

                            int fatherlength = father.Length - MyStag.FatherFirstName.Length;

                            //MyStag.FatherLastName = father.Trim().Substring(MyStag.FatherFirstName.Length, fatherlength).Trim().ToUpper();

                            MyStag.FatherLastName = father[0] +" "+ father[1];// father.Trim().Substring(father.TrimStart().IndexOf(" ")).Trim();
                            //FarmerName[1].Trim().Substring(FarmerName[1].IndexOf(' ')).Trim().ToUpper();


                            String[] Dosage = data_objects[21].Trim().Split(' ');
                            MyStag.PesticideDosage = Convert.ToDouble(Dosage[0].Trim());
                            if (Dosage.Length > 1)
                            {
                                MyStag.DosageUnit = Dosage[1].Trim().ToUpper();
                            }

                            string[] disease = new string[2]{null,null};
                            disease = data_objects[22].Trim().Split(' ');


                            MyStag.CLCVDisease = disease[0].Trim();
                            MyStag.CLCVDiseaseLevel = null;
                            if (disease.Length > 1)
                            {
                                MyStag.CLCVDiseaseLevel = disease[1].Trim().ToUpper();
                            }


                            String[] Height = data_objects[23].Trim().Split();
                            MyStag.PlantHeight = Convert.ToDouble(Height[0].Trim());
                            if (Height.Length > 1)
                            {
                                MyStag.PlantHeightUnit = Height[1].Trim().ToUpper();
                            } 

                            String[] area = data_objects[3].Trim().Split(' ');
                            MyStag.Area = Convert.ToDouble(area[0].Trim());
                            MyStag.AreaUnit = area[1].Trim().ToUpper();
                            MyStag.VarietyOfCrop = data_objects[4].Trim().ToUpper();
                           
                            string[] SowingDate;
                            string[] VisitDate;
                            string[] PesticideSprayDate;
                            char[] d = { '/' };
                            SowingDate = data_objects[5].Split(d);
                            VisitDate = data_objects[6].Split(d);

                            for (int k= 7; k < 19; k++)
                            {
                                if (data_objects[k].Trim() != "-")
                                {
                                    MyStag.PestPopulation[k-7]=Convert.ToDouble(data_objects[k].Trim());
                                }
                            }
                            MyStag.PesticideName = data_objects[19].Trim().ToUpper();
/*                                MyStag.PestPopulation1 = Convert.ToDouble(data_objects[7].Trim());
                                MyStag.PestPopulation2 = Convert.ToDouble(data_objects[8].Trim());
                                MyStag.PestPopulation3 = Convert.ToDouble(data_objects[9].Trim());
                                MyStag.PestPopulation4 = Convert.ToDouble(data_objects[10].Trim());
                                MyStag.PestPopulation5 = Convert.ToDouble(data_objects[11].Trim());
                                MyStag.PestPopulation6 = Convert.ToDouble(data_objects[12].Trim());
                                MyStag.PestPopulation7 = Convert.ToDouble(data_objects[13].Trim());
                                MyStag.PestPopulation8 = Convert.ToDouble(data_objects[14].Trim());
                                MyStag.PestPopulation9 = Convert.ToDouble(data_objects[15].Trim());
                                MyStag.PestPopulation10 = Convert.ToDouble(data_objects[16].Trim());
                                MyStag.PestPopulation11 = Convert.ToDouble(data_objects[17].Trim());
                                MyStag.PestPopulation12 = Convert.ToDouble(data_objects[18].Trim());
  */
                            //}


                            
                            PesticideSprayDate = data_objects[20].Split(d);


                            try
                            {

                                DateTime[] dt = new DateTime[] 
                                { 
                                    new DateTime(Convert.ToInt32(SowingDate[2].Trim()), Convert.ToInt32(SowingDate[1].Trim()), Convert.ToInt32(SowingDate[0].Trim())), 
                                    new DateTime(Convert.ToInt32(VisitDate[2].Trim()), Convert.ToInt32(VisitDate[1].Trim()), Convert.ToInt32(VisitDate[0].Trim())), 
                                    new DateTime(Convert.ToInt32(PesticideSprayDate[2].Trim()), Convert.ToInt32(PesticideSprayDate[1].Trim()), Convert.ToInt32(PesticideSprayDate[0].Trim())) 
                                };
                                //, new DateTime(Convert.ToInt32(PesticideSprayDate[2]), Convert.ToInt32(PesticideSprayDate[1], Convert.ToInt32(PesticideSprayDate[2])}; 
                                MyStag.SowingDate = dt[0];//new DateTime( SowingDate[0] //Convert.ToDateTime(data_objects[5].Trim());
                                MyStag.VisitDate = dt[1];// Convert.ToDateTime(data_objects[6].Trim());
                                MyStag.PesticideSprayDate = dt[2];// Convert.ToDateTime(data_objects[20].Trim());
                            }
                            catch (ArgumentOutOfRangeException e)
                            {
                                continue;
                            }
                           
                            DataRow dr = MyDataTable.NewRow();
                            dr[MyStag.TableAllColmNames[0]] = MyStag.DistrictName;
                            dr[MyStag.TableAllColmNames[1]] = MyStag.TownName;
                            dr[MyStag.TableAllColmNames[2]] = MyStag.FarmerFirstName;
                            dr[MyStag.TableAllColmNames[3]] = MyStag.FarmerLastName;
                            dr[MyStag.TableAllColmNames[4]] = MyStag.FatherFirstName;
                            dr[MyStag.TableAllColmNames[5]] = MyStag.FatherLastName;
                            dr[MyStag.TableAllColmNames[6]] = MyStag.Area;
                            dr[MyStag.TableAllColmNames[7]] = MyStag.AreaUnit;
                            dr[MyStag.TableAllColmNames[8]] = MyStag.VarietyOfCrop;
                            dr[MyStag.TableAllColmNames[9]] = MyStag.SowingDate;
                            dr[MyStag.TableAllColmNames[10]] = MyStag.VisitDate;

                            for (int j = 11; j < 23; j++)
                            {
                                dr[MyStag.TableAllColmNames[j]] = MyStag.PestPopulation[j-11];
                            } 

                            dr[MyStag.TableAllColmNames[23]] = MyStag.PesticideName;
                            dr[MyStag.TableAllColmNames[24]] = MyStag.PesticideSprayDate;
                            dr[MyStag.TableAllColmNames[25]] = MyStag.PesticideDosage;
                            dr[MyStag.TableAllColmNames[26]] = MyStag.DosageUnit;
                            dr[MyStag.TableAllColmNames[27]] = MyStag.CLCVDisease;
                            dr[MyStag.TableAllColmNames[28]] = MyStag.CLCVDiseaseLevel;
                            dr[MyStag.TableAllColmNames[29]] = MyStag.PlantHeight;
                            dr[MyStag.TableAllColmNames[30]] = MyStag.PlantHeightUnit;

                            MyDataTable.Rows.Add(dr);
                            ProgressBarPage1.Value++;
                        }
                }
            }
        }
        
        void initializeDataTableHeader(DataTable dt)
        {
            for (int i = 0; i < MyStag.TableAllColmNames.Length; i++)
            {
             
                for(int j=0; j< MyStag.TableDateColmName.Length;j++)
                {
                    if(MyStag.TableDateColmName[j] == MyStag.TableAllColmNames[i])
                    {
                        dt.Columns.Add(new DataColumn(MyStag.TableAllColmNames[i],typeof(DateTime)));
                        break;
                    }
                }

                for(int j=0; j< MyStag.TableStringColmNames.Length;j++)
                {
                    if(MyStag.TableStringColmNames[j] == MyStag.TableAllColmNames[i])
                    {
                        dt.Columns.Add(new DataColumn(MyStag.TableAllColmNames[i], typeof(String)));
                        break;
                    }
                }
                for(int j=0; j< MyStag.TableNumericColmNames.Length;j++)
                {
                    if(MyStag.TableNumericColmNames[j] == MyStag.TableAllColmNames[i])
                    {
                        dt.Columns.Add(new DataColumn(MyStag.TableAllColmNames[i], typeof(float)));
                        break;
                    }
                }
            }
        }

        DataTable MyDataTable = new DataTable("STAG");

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

            ProgressBarPage1.Maximum =dlg.FileName.Length;
            ProgressBarPage1.IsEnabled = true;

            ProgressBarPage1.Visibility =System.Windows.Visibility.Visible;

            char []Delimeter = split_criteria.Text.ToCharArray();


            initializeDataTableHeader(MyDataTable);

            ReadnTransfomFile(Delimeter, dlg.FileNames);

            grig.ItemsSource = MyDataTable.AsDataView();

            

                // grig.ItemsSource = obj.getdb().getStagAdapter().GetData();            //datagrid instance grig set to view data from database

                // Enable_Radio_Button();

                //setColmName();
            
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            using (SqlConnection dbConnection = new SqlConnection(DwhProject.Properties.Settings.Default.SikhConnectionString1))
            {
                dbConnection.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = MyDataTable.TableName;

                    foreach (var column in MyDataTable.Columns)
                    {
                        s.ColumnMappings.Add(column.ToString(), column.ToString());
                    }

                    try
                    {
                        s.WriteToServer(MyDataTable);
                        MessageBoxResult result = MessageBox.Show("SUCCESS");
                        
                    }
                    catch (Exception E)
                    {
                        MessageBoxResult result = MessageBox.Show(E.ToString()+"BulkInsert UNSUCCESSFUL");
                    }
                }
                dbConnection.Close();
            }
        }

        private void CubeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Form1 obj = new Form1();
            obj.ShowDialog();
            
        }

        private void ComboBoxDistinct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
