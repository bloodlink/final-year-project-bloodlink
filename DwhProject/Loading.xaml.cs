﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using DwhProject.SikhDataSetTableAdapters;
using System.Data.SqlClient;
using System.Data;


namespace DwhProject
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class Loading : Window
    {
        LoadingController LController = new LoadingController();

        public Loading()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int result = LController.LoadDimensionModel();

            MessageBoxResult res = MessageBox.Show(result.ToString());
        }
    }
}
