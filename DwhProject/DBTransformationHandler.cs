﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DwhProject.SikhDataSetTableAdapters;
using System.Data.SqlClient;

namespace DwhProject
{
    class DBTransformationHandler
    {
        StagObject MyStag;


        DWHAdapter dwhadapter;
        string connectionString;
        SqlConnection con;

        public DBTransformationHandler()
        {
            dwhadapter = new DWHAdapter();
            connectionString = DwhProject.Properties.Settings.Default.SikhConnectionString1;
            con = new SqlConnection(connectionString);
           // con.Open();
        }

        public void Connect()
        {
            if (con.State != ConnectionState.Open)
            {
                this.con.ConnectionString = this.connectionString;
                con.Open();
            }
 
        }

        public List<string> selectFromLookUpTable(String ColmnName, String LookUpTable)
        {
            List<String> MyList = new List<string>();

            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT (" + ColmnName + ") from [dbo].["+ LookUpTable+ "]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        MyList.Add(reader.GetString(0));
                    }

                    return MyList;
                }
            }
        }

        




        public List<string> selectAgregateFromTableOrderBy(String Agregate, String ColNames, String TableName, String OrderBy)
        {
            String[] ColmnName = ColNames.Split(',');

            List<String> MyList = new List<string>();

            String QUERY = "SELECT " + Agregate + " ";
            for (int i = 0; i < ColmnName.Length; i++)
            {
                QUERY += ColmnName[i];
                if (i < ColmnName.Length - 1)
                {
                    QUERY += ",";
                }
            }
            QUERY += " from [dbo].[" + TableName + "] ORDER BY  " + OrderBy;


            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand(QUERY, con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        String row = null;

                        for (int j = 0; j < ColmnName.Length; j++)
                        {
                            if (ColmnName[j] == "ID")
                            {
                                row += reader.GetInt64(j);
                            }
                            else if (ColmnName[j] == "VisitDate" || ColmnName[j] == "SowingDate" || ColmnName[j] == "PesticideSprayDate")
                            {
                                row += reader.GetDateTime(j);
                            }
                            else
                            {
                                try
                                {
                                    row += reader.GetString(j);
                                }
                                catch
                                {
                                }
                            }

                            if (j < ColmnName.Length - 1 && ColmnName.Length > 1)
                            {
                                row += ",";
                            }
                        }
                        MyList.Add(row);
                    }

                }
            }
            return MyList;
        }
        public List<string> selectAgregateFromTable(String Agregate,String ColNames, String TableName)
        {
            String[] ColmnName = ColNames.Split(',');

            List<String> MyList = new List<string>();

            String QUERY = "SELECT " + Agregate +  " ";
            for (int i = 0; i < ColmnName.Length; i++)
            {
                QUERY += ColmnName[i];
                if (i < ColmnName.Length - 1)
                {
                    QUERY += ",";
                }
            }
            QUERY += " from [dbo].[" + TableName + "]";


            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand(QUERY, con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        String row = null;

                        for (int j = 0; j < ColmnName.Length; j++)
                        {
                            if (ColmnName[j] == "ID")
                            {
                                row += reader.GetInt64(j);
                            }
                            else if (ColmnName[j] == "VisitDate" || ColmnName[j] == "SowingDate" || ColmnName[j] == "PesticideSprayDate")
                            {
                                row += reader.GetDateTime(j);
                            }
                            else
                            {
                                try
                                {
                                    row += reader.GetString(j);
                                }
                                catch
                                {
                                }
                            }

                            if (j < ColmnName.Length - 1 && ColmnName.Length > 1)
                            {
                                row += ",";
                            }
                        }
                        MyList.Add(row);
                    }

                }
            }
            return MyList;
        }

        public List<string> selectFromTable(String ColNames, String TableName)
        {
            String [] ColmnName = ColNames.Split(',');

            List<String> MyList = new List<string>();

            String QUERY = "SELECT ";
            for (int i = 0; i < ColmnName.Length; i++)
            {
                QUERY += ColmnName[i];
                if(i<ColmnName.Length-1)
                {
                    QUERY += ",";
                }
            }
             QUERY +=" from [dbo].[" + TableName + "]";


            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand(QUERY, con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        String row=null;

                        for (int j = 0; j < ColmnName.Length; j++)
                        {
                            if (ColmnName[j] == "Id" || ColmnName[j] == "ID")
                            {
                                row += reader.GetInt64(j);
                            }
                            else if (MyStag.TableDateColmName.Contains(ColmnName[j]))
                            {
                                row += reader.GetDateTime(j);
                            }
                            else
                            {
                                row += reader.GetString(j);
                            }

                            if (j < ColmnName.Length - 1 && ColmnName.Length>1)
                            {
                                row += ",";
                            }
                        }
                        MyList.Add(row);
                    }

                }
            }
            return MyList;
        }



        public int insertIntoTable(String ColNames, String Values, String TableName)
        {
            String[] ColmnName = ColNames.Split(',');
            String[] Val = Values.Split(',');


            List<String> MyList = new List<string>();

            String QUERY = "INSERT INTO [dbo].[" + TableName + "] (";

            for (int i = 0; i < ColmnName.Length; i++)
            {
                QUERY += ColmnName[i];
                if (i < ColmnName.Length - 1)
                {
                    QUERY += ",";
                }
            }
            QUERY += ") VALUES ( '";

            for (int i = 0; i < ColmnName.Length; i++)
            {
                QUERY += Val[i];
                if (i < Val.Length - 1)
                {
                    QUERY += "','";
                }
            }
            QUERY += "')";

            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand(QUERY, con))
                {
                    return command.ExecuteNonQuery();
                }
            }
            
        }

        public int ExecuteQuery(String QUERY)
        {
           
            
            using (SqlCommand command = new SqlCommand(QUERY, con))
            {
                
                return command.ExecuteNonQuery();
            }

        }


        public void loadLookUpTableValue(String ColmnName, String SourceTable, String LookUpTable)
        {
//            ColmnName = "FarmerName";

            if (ColmnName == "FarmerName")
            {
                List<String> AllFarmerNames = this.selectFromTable("FarmerName", "STAG");
                String[] AllFrameArray = AllFarmerNames.ToArray();

                String[] Delim = new String[] { "S/O", "s/o", "S/o", "s/O" };

                this.con.Close();
                this.con.ConnectionString = this.connectionString;
                this.con.Open();

                for (int i = 0; i < AllFrameArray.Length; i++)
                {
                    String[] SplittedNames = AllFrameArray[i].Split(Delim, StringSplitOptions.None);

                    for (int j = 0; j < SplittedNames.Length; j++)
                    {
                        String FarmerFirstName = SplittedNames[0].Split(' ')[0].Trim();
                        String FarmerLastName = SplittedNames[0].Substring(SplittedNames[0].IndexOf(' ')).Trim();

                        String FatherFirstName = SplittedNames[1].Split(' ')[0].Trim();
                        String FatherLastName = SplittedNames[1].Substring(SplittedNames[1].IndexOf(' ')).Trim();

                        this.ExecuteQuery("INSERT INTO [TempTable] (Value) values ('" + FarmerFirstName + "')");
                    }

                    //this.ExecuteQuery("DELETE FROM [TempTable]");
                }

            }
            else
            {
                List<String> All = selectAgregateFromTableOrderBy("DISTINCT", ColmnName, SourceTable,ColmnName);
                String[] AllRecords = All.ToArray();

                Connect();
                for (int i = 0; i < AllRecords.Length; i++)
                {
                    this.ExecuteQuery("INSERT INTO [" + LookUpTable + "] (Value) values ('" + AllRecords[i] +"')");
                }
                
            }
        }
        
    }
}
