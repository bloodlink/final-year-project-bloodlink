﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using DwhProject.SikhDataSetTableAdapters;
using System.Data.SqlClient;
using System.Data;

namespace DwhProject
{
    /// <summary>
    /// Interaction logic for Transformation.xaml
    /// </summary>
    public partial class Transformation : Window
    {
        DataTable MyDataTable;

        TransformationController TController;
        List<String> MyList= new List<string>();
        String[] TransformationColmNames = new String[]
        {
                  "DistrictName",
                    "TownName",
                    "FarmerFirstName",
                    "FarmerLastName",
                    "FatherFirstName",
                    "FatherLastName",
                    "AreaUnit",
                    "SowingDate",
                    "VisitDate",
                    "PesticideName", 
                    "PesticideSprayDate", 
                    "PesticideDosageUnit",
                    "CLCVDiseaseLevel",
                    "PlantHeightUnit"
        };
        public Transformation(DataTable Dt)
        {
            InitializeComponent();
            TController = new TransformationController();
            MyDataTable = Dt;

            
        }

        public void FillComboBoxWithLookUpTableNames()
        {
            for (int i = 0; i < TransformationColmNames.Length; i++)
            {
                ComboBox_SelectLookUp.Items.Add("LookUp_" + TransformationColmNames[i]);

            }
        }

        public void RefreshLookUpTables()
        {
            for (int i = 0; i < TransformationColmNames.Length; i++)
            {
                ComboBox_SelectLookUp.Items.Add("LookUp_" + TransformationColmNames[i]);
                TController.getDBHandler().Connect();
                TController.getDBHandler().ExecuteQuery("DELETE FROM " + "LookUp_" + TransformationColmNames[i]);
                TController.loadLookUpTableValue(TransformationColmNames[i],"STAG", "LookUp_" + TransformationColmNames[i]);
            }
        }

        private void ComboBox_SelectLookUp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            String LookUpTable = ComboBox_SelectLookUp.SelectedItem.ToString();
            MyList.Clear();

            MyList = TController.getDBHandler().selectFromLookUpTable("Value", LookUpTable);
            ListBox_SourceValues.SelectionMode = SelectionMode.Multiple;

   
           ListBox_SourceValues.Items.Clear();
           for (int i = 0; i < MyList.Count; i++)
           {
               ListBox_SourceValues.Items.Add(MyList[i]);
           }
           
     //       ListBox_MySelectedValues.ItemsSource = ListBox_SourceValues.SelectedItems;

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TController.DoAutoTransformation();
            //DBTransformationHandler db = TController.getDBHandler();
            //db.Connect();
            //TController.UpdateStagFromLookUpTables();
            //TController.TransformDosage();

        }



        private void Button_Forward_Click(object sender, RoutedEventArgs e)
        {

            String[] arr = new String[ListBox_SourceValues.SelectedItems.Count+1];
            
            ListBox_SourceValues.SelectedItems.CopyTo(arr,0);

            for (int i = 0; i <= ListBox_SourceValues.SelectedItems.Count; i++)
            {
                ListBox_SourceValues.Items.Remove(arr[i]);
                ListBox_MySelectedValues.Items.Add(arr[i]);
                
            }
        }

        private void Button_UpdateLookUpTables_Click(object sender, RoutedEventArgs e)
        {

            String Replacement = TextBox_Replacement.Text;
            DBTransformationHandler db = new DBTransformationHandler();
            db.Connect();

            for(int i=0; i< ListBox_MySelectedValues.Items.Count;i++)
            {
                Object item = ListBox_MySelectedValues.Items.GetItemAt(i);

                String Selected = null;
                if (item != null)
                {
                     Selected = item.ToString();
                }
            
                String QUERY= "UPDATE " + ComboBox_SelectLookUp.Text + " SET Replacement= '" + Replacement +"' WHERE Value= '" + Selected + "'"  ;

                int x = db.ExecuteQuery(QUERY);

                if (x > 0)
                {
                    ListBox_MySelectedValues.Items.RemoveAt(i);
                }
            }
           
        }

        private void Button_Backward_Click(object sender, RoutedEventArgs e)
        {
            String[] arr = new String[ListBox_MySelectedValues.SelectedItems.Count+1];
            ListBox_MySelectedValues.SelectedItems.CopyTo(arr, 0);

            for (int i = 0; i <= ListBox_MySelectedValues.SelectedItems.Count; i++)
            {
                ListBox_MySelectedValues.Items.Remove(arr[i]);
                ListBox_SourceValues.Items.Add(arr[i]);
                
            }
        }

        private void TransformDosage()
        {
            TController.TransformDosage();
        }

        public void UpdateStagFromLookUpTables()
        {
            TController.UpdateStagFromLookUpTables("STAG");
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            UpdateStagFromLookUpTables();
            TransformDosage();



            /*
            if (ListBox_SourceValues.Items.Count == 0)
            {
                string sMessageBoxText = "Do you want to set replacement of remaining ojects with themselves?";
                string sCaption = "Remaining Distinct Values";

                MessageBoxButton btnMessageBox = MessageBoxButton.YesNoCancel;
                MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

                MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

                switch (rsltMessageBox)
                {
                    case MessageBoxResult.Yes:
                        
                        for(int i=0; i< ListBox_SourceValues.SelectedItems.Count;i++)
                        {
                            Object item = ListBox_SourceValues.Items.GetItemAt(i);
                            String Selected = item.ToString();
            
                            String QUERY= "UPDATE " + ComboBox_SelectLookUp.Text + " SET Replacement= '" + Selected +"' WHERE Value= '" + Selected + "'"  ;

                            DBTransformationHandler db = new DBTransformationHandler();
                            db.ExecuteQuery(QUERY);

                        }            
                        break;

                    case MessageBoxResult.No:
                        /* ... */
              //          break;

                  //  case MessageBoxResult.Cancel:
                        /* ... */
                //        break;
           //     }
           // }

//            */
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            DBTransformationHandler db = new DBTransformationHandler();

            db.Connect();
            for (int i = 0; i < ListBox_SourceValues.Items.Count; i++)
            {
                Object item = ListBox_SourceValues.Items[i];

                String Selected = null;
                if (item != null)
                {
                    Selected = item.ToString();
                }

                String QUERY = "UPDATE " + ComboBox_SelectLookUp.Text + " SET Replacement= '" + Selected + "' WHERE Value= '" + Selected + "'";

                int x = db.ExecuteQuery(QUERY);

                if (x > 0)
                {
                    ListBox_SourceValues.Items.RemoveAt(i);
                }
            }
           

        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            RefreshLookUpTables();
        }

        
    }
}
