﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DwhProject.SikhDataSetTableAdapters;
using System.Data.SqlClient;

namespace DwhProject
{
    class DBLoadingController
    {

        StagObject MyStag= new StagObject();
        string StagConectionString;
        string DMconnectionString;
        string SFconnectionString;

        SqlConnection DMcon;
        SqlConnection STAGcon;
        SqlConnection SFcon;

        public DBLoadingController()
        {
            DMcon = new SqlConnection();
            STAGcon = new SqlConnection();

            StagConectionString = DwhProject.Properties.Settings.Default.SikhConnectionString1;
            DMconnectionString = DwhProject.Properties.Settings.Default.DMConnectionString;

            Connect();
        }

        public void Connect()
        {

            DMcon.ConnectionString = DMconnectionString;//"Data Source=(LocalDB)\\v11.0;AttachDbFilename=F:\\Projects\\dwh\\DwhProject\\DwhProject\\DM.mdf;Integrated Security=True;Connect Timeout=30";
            STAGcon.ConnectionString = StagConectionString;
            DMcon.Open();
            STAGcon.Open();
        }

        private int LoadDT_Farmer()
        {
            String QUERY = "INSERT INTO [dbo].[DT_Farmer] (FarmerFirstName ,FarmerLastName, FatherFirstName, FatherLastName)"+
                " SELECT DISTINCT FarmerFirstName ,FarmerLastName, FatherFirstName, FatherLastName "+
                "FROM [dbo].[STAG] ORDER BY FarmerFirstName";

            using (SqlCommand command = new SqlCommand(QUERY, STAGcon))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                return reader.RecordsAffected;
            }
        }

        private int LoadDT_Date()
        {
            String QUERY = "INSERT INTO DT_Date (Id, Year,Quarter, Month, Day, MonthName) SELECT   DISTINCT Result as Id, DATEPART(YEAR,RESULT) as Year ,DATENAME(QUARTER,RESULT) as Quarter,DATEPART(MONTH,RESULT)as Month, DATEPART(DAY,RESULT) as Day,DATENAME(MONTH,RESULT) as MonthName"+
                "( SELECT    VisitDate, SowingDate, PesticideSprayDate FROM [dbo].[STAG])p" +
                "UNPIVOT ( Result FOR SubjectName  in (VisitDate, SowingDate, PesticideSprayDate))unpvt";

            using (SqlCommand command = new SqlCommand(QUERY, STAGcon))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                return reader.RecordsAffected;
            }
        }

        private int LoadDT_Location()
        {
            String QUERY = "INSERT INTO DT_Location (DistrictName,TownName) " +
                "SELECT DISTINCT DistrictName,TownName FROM	[dbo].[STAG]";

            using (SqlCommand command = new SqlCommand(QUERY, STAGcon))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                return reader.RecordsAffected;
            }
        }

        private int LoadDT_Pesticide()
        {
            String QUERY = "INSERT INTO DT_Pesticide(PesticideName) SELECT DISTINCT PesticideName FROM [dbo].[STAG]";

            using (SqlCommand command = new SqlCommand(QUERY, STAGcon))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                return reader.RecordsAffected;
            }
        }

        private int LoadDT_Units()
        {
            String QUERY= "INSERT INTO DT_Unit (UnitSymbol) SELECT DISTINCT PlantHeightUnit from [dbo].[STAG] union"+
                         "SELECT DISTINCT AreaUnit from [dbo].[STAG]"+
                            "union SELECT DISTINCT PesticideDosageUnit from [dbo].[STAG]";
            
            using (SqlCommand command = new SqlCommand(QUERY, STAGcon))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                return reader.RecordsAffected;
            }
        }
        
        private int LoadDimensions()
        {
             LoadDT_Farmer();
             LoadDT_Date();
             LoadDT_Location();
             LoadDT_Pesticide();
             LoadDT_Units();
             return 1;
        }

        private int LoadFactTable()
        {

            return 0;
        }

        public int LoadDimensionModel()
        {
            
            //LoadFactTable();

            return LoadDimensions();
        }

        public int LoadSnowFlake()
        {
            return 0;   
        }

    }
}

