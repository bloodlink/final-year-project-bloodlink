﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DwhProject
{
    class TransformationController
    {
        DBTransformationHandler DBHandler;

        StagObject MyStag = new StagObject();

        public TransformationController()
        {
            DBHandler = new DBTransformationHandler();
           
        }
        public DBTransformationHandler getDBHandler()
        {
            return this.DBHandler;
        }

        public List<String> getDataFromLookUpTable(String ColmnName, String LookUpTable)
        {
            return DBHandler.selectFromLookUpTable(ColmnName, LookUpTable);

        }

        public void loadLookUpTableValue(String ColmnName, String SourceTable, String LookUpTable)
        {
            DBHandler.loadLookUpTableValue(ColmnName,SourceTable,LookUpTable);
        }

        public void TransformDosage()
        {
            DBHandler.Connect();
            DBHandler.ExecuteQuery(" UPDATE [dbo].[STAG]"+
                                    " SET PesticideDosage = 1000* PesticideDosage,PesticideDosageUnit='" + MyStag.PesticideDosageUnitSymbol + "' " +
                                    " WHERE PesticideDosageUnit='ml' "+
                                    " UPDATE [dbo].[STAG] " +
                                    " SET PesticideDosage = 1.04* PesticideDosage, PesticideDosageUnit='" + MyStag.PesticideDosageUnitSymbol + "' " +
                                    " WHERE PesticideDosageUnit='Kg' "+
                                    " UPDATE [dbo].[STAG] " +
                                    " SET PesticideDosage = 0.001*1.04*PesticideDosage, PesticideDosageUnit='" + MyStag.PesticideDosageUnitSymbol + "' " +
                                   "  WHERE PesticideDosageUnit='grams' ");

        }

        public void TransformPlantHeight()
        {
            DBHandler.Connect();
            DBHandler.ExecuteQuery(" UPDATE [dbo].[STAG]"+
                                    " SET PlantHeight = 100* PlantHeight,PlantHeightUnit='" + MyStag.PlantHeightUnitSymbol + "' " +
                                    " WHERE PlantHeightUnit='cm' "+
                                    " UPDATE [dbo].[STAG] " +
                                    " SET PlantHeight = 0.3048* PlantHeight, PlantHeightUnit='" + MyStag.PlantHeightUnitSymbol + "' " +
                                    " WHERE PlantHeightUnit='foot' "+
                                    " UPDATE [dbo].[STAG] " +
                                    " SET PlantHeight = 0.0254*PlantHeight, PlantHeightUnit='"+MyStag.PlantHeightUnitSymbol+"' "+
                                   "  WHERE PlantHeightUnit='inch'");
        }


        public void ImputationForCoulmn(String ColmnName, String TableName)
        {

            DBHandler.Connect();
            DBHandler.ExecuteQuery("UPDATE [dbo].["+TableName+"] SET "+ColmnName+"=(SELECT MAX("+ColmnName+") from [dbo].["+TableName+"]) ");
        }


        public void UpdateStagFromLookUpTables(String TableName)
        {
            DBHandler.Connect(); 
            for (int i = 0; i < MyStag.TableLookUpColmNames.Length; i++)
            {
                String QUERY = " UPDATE [dbo].["+TableName+"] SET " + MyStag.TableLookUpColmNames[i] + " = (SELECT TOP 1 t.Replacement FROM LookUp_" + MyStag.TableLookUpColmNames[i] + " t WHERE STAG." + MyStag.TableLookUpColmNames[i] + " = t.Value)";
                DBHandler.ExecuteQuery(QUERY);

            }
        }

        public List<object> Imputationfunction(List<object> arrayListToBeImputed)
        {
            for (int i = 0; i < arrayListToBeImputed.Count; i++)
            {
                if (arrayListToBeImputed[i] == null)
                {
                    arrayListToBeImputed[i] = arrayListToBeImputed.Max();
                }
            }

            return arrayListToBeImputed;
        }

        public void AutoUpdateLookUpTables()
        {
            List<String> FirstnameTemp = DBHandler.selectAgregateFromTable("DISTINCT", "FarmerFirstName", "STAG");
            FirstnameTemp.Sort();
            List<String> FirstNameList = AutoTransformationCoulumns(FirstnameTemp.OfType<object>().ToList(), FirstnameTemp.OfType<object>().ToList());
            
            for(int i=0;i< FirstnameTemp.Count;i++)
            {
                String QUERY = " UPDATE [dbo].[LookUp_FarmerFirstName] SET Value = "+FirstnameTemp[i]+", Replacement ="+FirstNameList[i]+" where Value = "+FirstnameTemp[i]+"";
            }
        }

        public void DoAutoTransformation()
        {
            //this.TransformPlantHeight();
            //this.TransformDosage();
            //this.ImputationForCoulmn("CLCVDiseaseLevel","STAG");


        }


        public List<String> AutoTransformationCoulumns(List<object> ListAllValues, List<object> ListDistinctValues)
        {
            int list1Length;
            int list2Length;
            int commonItemLength;

            for (int i = 0; i < ListAllValues.Count; i++)
            {
                for (int j = 0; j < ListDistinctValues.Count; j++)
                {
                    if (ListAllValues[i].ToString() != ListDistinctValues[j].ToString())
                    {
                        var commonList = ListAllValues[i].ToString().Intersect(ListDistinctValues[j].ToString());

                        list1Length = ListAllValues[i].ToString().Length;
                        list2Length = ListDistinctValues[j].ToString().Length;
                        commonItemLength = commonList.Count();

                        double similarity = 100 * (commonItemLength * 2) / (list1Length + list2Length);
                        if (similarity>80)
                        {
                            ListAllValues[i] = ListDistinctValues[j];
                        }
                    }
                }

            }

            return ListAllValues.OfType<String>().ToList();

        }

        public void LoadProfileListInProfileTable(List<int> myDistinctList, List<int> myNotNullList, List<int> myNullList, List<int> myInvalidList, List<double> myMinList, List<double> myMaxList, List<double> myAvgList, List<DateTime> myMinDatelist, List<DateTime> myMaxDatelist)
        {
            
        }

    }
}
