﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DwhProject
{
    class StagObject
    {
        public String PlantHeightUnitSymbol = "m";
        public String PesticideDosageUnitSymbol = "L";

        void function()
        {
        //    String.Compare(DistrictName
        }

        public String DistrictName,TownName,FarmerFirstName, FarmerLastName, FatherFirstName, FatherLastName;
        public Double Area;
        public String AreaUnit;
        public String VarietyOfCrop;
        public DateTime SowingDate;

        public DateTime VisitDate;
        public  Double [] PestPopulation = new Double[12];


        
        public String PesticideName;
        public DateTime PesticideSprayDate;
        public Double PesticideDosage;
        public String DosageUnit;
        public String CLCVDisease;
        public String CLCVDiseaseLevel;
        public Double PlantHeight;
        public String PlantHeightUnit;


        public String[] TableAllColmNames = new String[]
        {
                    "DistrictName",
                    "TownName",
                    "FarmerFirstName",
                    "FarmerLastName",
                    "FatherFirstName",
                    "FatherLastName",
                    "Area",
                    "AreaUnit",
                    "VarietyOfCrop",
                    "SowingDate",
                    "VisitDate",
                    "PestPopulation1",
                    "PestPopulation2",
                    "PestPopulation3",
                    "PestPopulation4",
                    "PestPopulation5",
                    "PestPopulation6",
                    "PestPopulation7",
                    "PestPopulation8",
                    "PestPopulation9",
                    "PestPopulation10",
                    "PestPopulation11",
                    "PestPopulation12",
                    "PesticideName", 
                    "PesticideSpraydate", 
                    "PesticideDosage",
                    "PesticideDosageUnit",
                    "CLCVDisease",
                    "CLCVDiseaseLevel",
                    "PlantHeight",
                    "PlantHeightUnit"
        };
        public String[] TableLookUpColmNames = new String[]
        {
                    "DistrictName",
                    "TownName",
                    "FarmerFirstName",
                    "FarmerLastName",
                    "FatherFirstName",
                    "FatherLastName",
                    "AreaUnit",
                    "SowingDate",
                    "VisitDate",
                    "PesticideName", 
                    "PesticideSpraydate", 
                    "PesticideDosageUnit",
                    "CLCVDiseaseLevel",
                    "PlantHeightUnit"
        };

        public String[] TableStringColmNames = new String[]
        {
                  "DistrictName",
                    "TownName",
                    "FarmerFirstName",
                    "FarmerLastName",
                    "FatherFirstName",
                    "FatherLastName",
                    "AreaUnit",
                    "VarietyOfCrop",
                    "PesticideName", 
                    "PesticideDosageUnit",
                    "CLCVDiseaseLevel",
                    "PlantHeightUnit"
        };

        public String[] TableDateColmName = new String[]
        {
                     "SowingDate",
                    "VisitDate",
                    "PesticideSpraydate"
        };

        public String[] TableNumericColmNames = new String[]
        {
                    "Area",
                    "PestPopulation1",
                    "PestPopulation2",
                    "PestPopulation3",
                    "PestPopulation4",
                    "PestPopulation5",
                    "PestPopulation6",
                    "PestPopulation7",
                    "PestPopulation8",
                    "PestPopulation9",
                    "PestPopulation10",
                    "PestPopulation11",
                    "PestPopulation12",
                    "PesticideDosage",
                    "CLCVDisease",
                    "PlantHeight",
        };

        

    }
}
