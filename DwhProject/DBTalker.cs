﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DwhProject.SikhDataSetTableAdapters;
using DwhProject;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace DwhProject
{
    class DBTalker
    {
        DWHAdapter ta;
        STAGTableAdapter SA;
        
        string connectionString;
        SqlConnection con;

        StagObject MyStag = new StagObject();
      

           
       public  DBTalker()
        {
            SA = new STAGTableAdapter();
            //ta = new DWHAdapter();
            connectionString = DwhProject.Properties.Settings.Default.SikhConnectionString1;
            con = new SqlConnection(connectionString);
            con.Open();
        }

       public SqlConnection getConnection()
       {
           return con;
       }
        public STAGTableAdapter getStagAdapter()
        {
            return SA;
        }

       private void check(string [] array)
       {
           for (int i = 0; i < array.Length;i++)
           {
               if ((array[i] == "-" && i == 5) || (array[i] == "-" && i == 6) || (array[i] == "-" && i == 20))
               {
                 
                   array[i] = Convert.ToString(default(DateTime));
               }

               else if (array[i] == "-")
               {
                   array[i] = "-1";
               }

               else
               {

               }
           }
           
       }
        
       public void inserter(string[] data_objects)
        {
            String[] Delim = new String[] { "S/O", "s/o", "S/o", "s/O" };

            try
            {
                String[] FarmerName = data_objects[2].Trim().Split(Delim, StringSplitOptions.None);

                String FarmerFirstName = FarmerName[0].Trim().Split(' ')[0].Trim();
                String FarmerLastName = FarmerName[0].Trim().Substring(FarmerName[0].IndexOf(' ')).Trim();

                String FatherFirstName = FarmerName[1].Trim().Split(' ')[0].Trim();
                String FatherLastName = FarmerName[1].Trim().Substring(FarmerName[0].IndexOf(' ')).Trim();


                String[] Dosage = data_objects[21].Trim().Split(' ');
                Double PesticideDosage = Convert.ToDouble(Dosage[0].Trim());
                String DosageUnit = Dosage[1].Trim();

                string[] disease = new string[2]{null,null};
                disease = data_objects[22].Trim().Split(' ');


                String CLCVDisease = disease[0].Trim();
                String CLCVDiseaseLevel = null;
                CLCVDiseaseLevel = disease[1].Trim();

                String[] Height = data_objects[23].Trim().Split();

                Double PlantHeight = Convert.ToDouble(Height[0].Trim());
                String PlantHeightUnit = Height[1].Trim();

                String[] area = data_objects[3].Trim().Split(' ');
                Double Area = Convert.ToDouble(area[0].Trim());
                String AreaUnit = area[1].Trim();

                string[] SowingDate;
                string[] VisitDate;
                string[] PesticideSprayDate;
                char[] d = { '/' };
                SowingDate = data_objects[5].Split(d);
                VisitDate = data_objects[6].Split(d);
                PesticideSprayDate = data_objects[20].Split(d);
                check(data_objects);
            

            DateTime[] dt = new DateTime[] { new DateTime(Convert.ToInt32(SowingDate[2]), Convert.ToInt32(SowingDate[1]), Convert.ToInt32(SowingDate[0])), new DateTime(Convert.ToInt32(VisitDate[2]), Convert.ToInt32(VisitDate[1]), Convert.ToInt32(VisitDate[0])), new DateTime(Convert.ToInt32(PesticideSprayDate[2]), Convert.ToInt32(PesticideSprayDate[1]), Convert.ToInt32(PesticideSprayDate[0])) };//, new DateTime(Convert.ToInt32(PesticideSprayDate[2]), Convert.ToInt32(PesticideSprayDate[1], Convert.ToInt32(PesticideSprayDate[2])};
            
           if (con.State == ConnectionState.Open && con.State != ConnectionState.Connecting)
            {
         /*       ta.InsertQuery(data_objects[0], data_objects[1], data_objects[2], data_objects[3], data_objects[4],
                   dt[0], dt[1], Convert.ToDouble(data_objects[7]), Convert.ToDouble(data_objects[8]), Convert.ToDouble(data_objects[9]), Convert.ToDouble(data_objects[10]),
                   Convert.ToDouble(data_objects[11]), Convert.ToDouble(data_objects[12]), Convert.ToDouble(data_objects[13]), 12, Convert.ToDouble(data_objects[15]), Convert.ToDouble(data_objects[16])
                   , Convert.ToDouble(data_objects[17]), Convert.ToDouble(data_objects[18]), data_objects[19], dt[2], data_objects[21],
                   data_objects[22], data_objects[23]);*/


                SA.InsertQuery(data_objects[0], data_objects[1], FarmerFirstName, FarmerLastName, FatherFirstName, FatherLastName, 
                    Area,AreaUnit, data_objects[4],dt[0], dt[1], Convert.ToDouble(data_objects[7]), Convert.ToDouble(data_objects[8]), 
                    Convert.ToDouble(data_objects[9]), Convert.ToDouble(data_objects[10]),Convert.ToDouble(data_objects[11]),
                    Convert.ToDouble(data_objects[12]), Convert.ToDouble(data_objects[13]), Convert.ToDouble(data_objects[14]), 
                    Convert.ToDouble(data_objects[15]), Convert.ToDouble(data_objects[16]),Convert.ToDouble(data_objects[17]),
                    Convert.ToDouble(data_objects[18]), data_objects[19], dt[2], PesticideDosage, DosageUnit, CLCVDisease, CLCVDiseaseLevel,
                    PlantHeight, PlantHeightUnit); 
                    
            }

            }
            catch (Exception e)
            {

            }
        }

       public int getTotalDistinct(string colmnName, string TableName)
       {
           
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT COUNT(DISTINCT(" + colmnName + ")) from [dbo].[" + TableName + "]", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       
                       return reader.GetInt32(0);
                   }
               }
           }
           return 0;
       }



       public double getMax(string colmnName, string TableName)
       {

           if (MyStag.TableDateColmName.Contains(colmnName) || MyStag.TableStringColmNames.Contains(colmnName))
           {
               return 0;
           }

           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT MAX(" + colmnName + ") from [dbo].[" + TableName + "]", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetDouble(0);
                   }
               }
           }
           return 0;
       }


       public DateTime getMaxDate(string colmnName,string TableName)
       {
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();

               using (SqlCommand command = new SqlCommand("SELECT MAX(" + colmnName + ") from [dbo].[" +TableName+ "]", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       
                       //int? stockvalue = (int?)(!Convert.IsDBNull(reader.GetInt32(0)) ? reader.GetInt32(0) : -1);
                       //if (reader[colmnName] != ColmNames.Contains(colmnName))
                       //return reader.GetDateTime(0);
                       
                       return Convert.ToDateTime(reader[0]);
                       //return Convert.ToInt32(reader[0]);
                       //return reader.GetDouble(0);


                   }
               }
           }
           return default(DateTime);
       }

       public DateTime getMinDate(string colmnName, string TableName)
       {
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();

               using (SqlCommand command = new SqlCommand("SELECT MIN(" + colmnName + ") from [dbo].[" + TableName + "]", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       //int? stockvalue = (int?)(!Convert.IsDBNull(reader.GetInt32(0)) ? reader.GetInt32(0) : -1);
                       //if (reader[colmnName] != ColmNames.Contains(colmnName))
                       //return reader.GetDateTime(0);
                       return Convert.ToDateTime(reader[0]);
                       //return Convert.ToInt32(reader[0]);
                       //return reader.GetDouble(0);


                   }
               }
           }
           return default(DateTime);
       }

       public double getMin(string colmnName, string TableName)
       {
           if (MyStag.TableDateColmName.Contains(colmnName) || MyStag.TableStringColmNames.Contains(colmnName))
           {
               return 0;
           }

           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();

               using (SqlCommand command = new SqlCommand("SELECT MIN(" + colmnName + ") from [dbo].[" + TableName + "]", con)) 
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       //int? stockvalue = (int?)(!Convert.IsDBNull(reader.GetInt32(0)) ? reader.GetInt32(0) : -1);
                       //if (reader[colmnName] != ColmNames.Contains(colmnName))
                       //return reader.GetDateTime(0);
                       return reader.GetDouble(0);
                       //return Convert.ToInt32(reader[0]);
                       //return reader.GetDouble(0);
                       
                       
                   }
               }
           }
           return 0;
       }





       public double getAvg(string colmnName, string TableName)
       {
           if (MyStag.TableDateColmName.Contains(colmnName) || MyStag.TableStringColmNames.Contains(colmnName))
           {
               return 0;
           }

           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT AVG(" + colmnName + ") from [dbo].[" + TableName + "]", con)) 
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetDouble(0);
                   }
               }
           }
           return 0;
       }



       public int getTotalNull(string colmnName, string TableName)
       {
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT COUNT(*)  FROM [dbo].["+ TableName+"] WHERE ("+ colmnName +" IS NULL)", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return 0;
       }
       public int getTotalNotNull(string colmnName, string TableName)
       {
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT COUNT(*)  FROM [dbo].[" + TableName + "] WHERE (" + colmnName + " IS NOT NULL)", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return 0;
       }

        public int getTotalInvalid(string colmnName, string TableName)
       {
           using (con)
           {
               con.Close();
               con.ConnectionString = this.connectionString;
               con.Open();
               using (SqlCommand command = new SqlCommand("SELECT COUNT(" + colmnName + ")  FROM [" + TableName + "] WHERE (" + colmnName + " LIKE '0');", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return -1;
       }
        public int getTotalRecordCount(string TableName)
        {
            using (con)
            {
                con.Close();
                con.ConnectionString = this.connectionString;
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT COUNT(*)  FROM [" + TableName+ "] ;", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        return reader.GetInt32(0);
                    }
                }
            }
            return -1;
        }

        public int InsertLoookUpValue(String SourceColmn,String SourceTable,String LookUpColmn,  String LookUpTable)
        {
            
           using (con)
           {
               using (SqlCommand command = new SqlCommand("INSERT into [" + LookUpTable + "] ("+ LookUpColmn +") Select DISTINCT "+ SourceColmn + "from [" +SourceTable+ "];", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return -1;
 
        }
        public void BulkInsert()
        {
            //SA.BULKINSERT("F:\\Semester7\\DataWarehouse\\Project\\DW-Fall2013-Semester Project-Dataset\\Shujabad\\Shujabad1.txt","$","\n");
            //SA.InsertQuery();
            //ta.Procedure_BulkInsert_Excel();
        }

        public int PROC()
        {
            String FileName= "F:\\Semester7\\DataWarehouse\\Project\\Dataset\\Shujabad\\Shujabad1.txt";
            String Delim = "$";

            System.IO.File.Open(FileName, System.IO.FileMode.Open);

           using (con)
           {
               using (SqlCommand command = new SqlCommand("BULK INSERT [dbo].[STAG] FROM '" + FileName +"' WITH ( FIELDTERMINATOR = '"+ Delim + "',ROWTERMINATOR = '\n')", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return -1;

        }

        public List<int> GetDistinctArray(String [] ColmNames, string TableName)
        {
         List<int> AlDistinctList = new List<int>();
         for (int i = 0; i < ColmNames.Length; i++)
         {
             AlDistinctList.Add(this.getTotalDistinct(ColmNames[i], TableName));
         } 
            
            return AlDistinctList;
        }



        public List<double> GetMaxArray(String[] ColmNames, string TableName)
        {
            List<double> AlMaxList = new List<double>();
            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlMaxList.Add(this.getMax(ColmNames[i], TableName));
            }

            return AlMaxList;
        }

        public List<double> GetMinArray(String[] ColmNames, string TableName)
        {
            List<double> AlMinList = new List<double>();
            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlMinList.Add(this.getMin(ColmNames[i], TableName));
            }

            return AlMinList;
        }

        public List<DateTime> GetMinDateArray(String[] ColmNames, string TableName)
        {
            List<DateTime> AlMinDateList = new List<DateTime>();
            for (int i = 0; i < MyStag.TableDateColmName.Length; i++)
            {
                AlMinDateList.Add(this.getMinDate(MyStag.TableDateColmName[i], TableName));
            }

            return AlMinDateList;
        }

        public List<DateTime> GetMaxDateArray(String[] ColmNames, string TableName)
        {
            List<DateTime> AlMaxDateList = new List<DateTime>();
            for (int i = 0; i < MyStag.TableDateColmName.Length; i++)
            {
                AlMaxDateList.Add(this.getMaxDate(MyStag.TableDateColmName[i], TableName));
            }

            return AlMaxDateList;
        }


        public List<Double> GetAvgArray(String[] ColmNames, string TableName)
        {
            List<double> AlAvgList = new List<double>();
            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlAvgList.Add(this.getAvg(ColmNames[i], TableName));
            }

            return AlAvgList;
        }



        public List<int> GetNULLArray(String[] ColmNames, string TableName)
        {
            List<int> AlNullList = new List<int>();

            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlNullList.Add(this.getTotalNull(ColmNames[i], TableName));
            } 

            return AlNullList;
        }

        public List<int> GetNotNULLArray(String[] ColmNames, string TableName)
        {
            List<int> AlNotNullList = new List<int>();

            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlNotNullList.Add(this.getTotalNotNull(ColmNames[i], TableName));
            }

            return AlNotNullList;
        }

        public List<int> GetInvalidArray(String[] ColmNames, string TableName)
        {
            List<int> AlInvalidList = new List<int>();

            for (int i = 0; i < ColmNames.Length; i++)
            {
                AlInvalidList.Add(this.getTotalInvalid(ColmNames[i], TableName));
            }

            return AlInvalidList;
        }



        public int LoaadExcel()
        {
            using (con)
           {
                String FileName = "F:\\Semester7\\DataWarehouse\\Project\\DW-Fall2013-Semester Project-Dataset\\Sample.xlsx"; 

               using (SqlCommand command = new SqlCommand("Select * INTO TmpTable FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', 'Excel 8.0;Database=" + FileName + ";HDR=YES', 'SELECT * FROM [SheetName$]')", con))
               using (SqlDataReader reader = command.ExecuteReader())
               {
                   while (reader.Read())
                   {

                       return reader.GetInt32(0);
                   }
               }
           }
           return -1;
        }
       public DWHAdapter show_data()
       {
           return ta; 
       }
    }
}
