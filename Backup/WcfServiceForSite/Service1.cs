﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceForSite
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
         
        Class1 obj = new Class1();
        private string str;
        private SqlConnection s;
        public Service1()
        {
             str = obj.connection();
             s = obj.get_connection();
             obj.Adding_in_lists();//loadingLists
           
        }

        public void insert(String UserName,String Password,String type,String Status)
        {
            String[] Input_Array = new string[] { UserName, Password, type, Status };

            SqlCommand com = new SqlCommand(obj.query_maker_insert("Profile", Input_Array), s);
            com.Connection.Close();
            com.Connection.Open();
            com.ExecuteReader();
            com.Connection.Close();
        }

        public List<String[]> GetData()
        {
            String b = null;
            String[] alpha = null;
            String[] beta = null;
            int count=0;
            List<String[]> mylist=new List<String[]>();
            ArrayList list=new ArrayList();
            List<String> storageList=new List<string>();
            string[] arr = new string[] { "D1", "D2" };//values to be given as input for update
            
            
            
            //string query = obj.query_maker_insert("Profile",arr);
            string query = obj.query_maker_select("Profile");
            String[] coulum_names = obj.get_coulumns().Split(',');
                            //string query = obj.query_maker_update("Profile", arr, "[Profile_ID]='4'");
            SqlCommand com = new SqlCommand(query, s);
              //com.ExecuteReader();
            com.Connection.Close();
           com = new SqlCommand(query, s);
            com.Connection.Open();
            SqlDataReader dr = com.ExecuteReader();
            if (dr.HasRows)
            {
                
                while (dr.Read())
                {
                    for (int i = 0; i < coulum_names.Length; i++)
                    {
                        if (i == 0)
                        {
                            storageList.Add(Convert.ToString(dr.GetInt64(i)));
                        }
                        else
                        {
                            storageList.Add(dr.GetString(i));
                        }
                    }
                    alpha = storageList.ToArray();
                    list.Add(alpha);
                    mylist.Add(alpha);
                    storageList.Clear();
                }
                com.Connection.Close();
            }
            
            
            //return string.Format("You entered: {0}", b);
            //return list.ToArray(typeof(String)) as String[]; ;
            return mylist;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
